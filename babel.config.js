module.exports = (api) => {
  api.cache(true);

  return {
    presets: ['module:metro-react-native-babel-preset'],
    plugins: [
      [
        'babel-plugin-root-import',
        {
          paths: [
            {
              rootPathPrefix: '~',
              rootPathSuffix: 'src',
            },
            {
              rootPathPrefix: '#',
              rootPathSuffix: 'res',
            },
          ],
        },
      ],
    ],
    env: {
      production: {
        plugins: [
          [
            'babel-plugin-root-import',
            {
              paths: [
                {
                  rootPathPrefix: '~',
                  rootPathSuffix: 'src',
                },
                {
                  rootPathPrefix: '#',
                  rootPathSuffix: 'res',
                },
              ],
            },
          ],
          'transform-remove-console',
        ],
      },
    },
  };
};
