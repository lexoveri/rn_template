import {
  createStore,
  applyMiddleware,
  compose,
  Middleware,
  Store,
} from 'redux';
import logger from 'redux-logger';
import createSagaMiddleware from 'redux-saga';
import { persistStore } from 'redux-persist';
import { composeWithDevTools } from 'redux-devtools-extension';

import { rootReducer, rootSaga } from '~/modules';
import { Persistor } from 'redux-persist/es/types';

const sagaMiddleware = createSagaMiddleware();

function configMiddleware(): Middleware[] {
  const middleware: Middleware[] = [];

  middleware.push(sagaMiddleware);

  if (__DEV__) {
    middleware.push(logger);
  }

  return middleware;
}

const composer = __DEV__ ? (composeWithDevTools as typeof compose) : compose;

export default () => {
  const middlewares = configMiddleware();

  const store: Store = createStore(
    rootReducer,
    composer(applyMiddleware(...middlewares)),
  );

  sagaMiddleware.run(rootSaga);

  const persistor: Persistor = persistStore(store);
  return { store, persistor };
};
