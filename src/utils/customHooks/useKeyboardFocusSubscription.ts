import { useEffect, useState } from 'react';
import { Keyboard, Platform } from 'react-native';

export default function useKeyboardFocusSubscription(
  didShowCallBack?: () => void,
  didHideCallback?: () => void,
): boolean {
  const [keyboardFocused, setKeyboardFocus] = useState(false);

  useEffect(() => {
    const didShowUnsubscribable = Keyboard.addListener(
      'keyboardDidShow',
      () => {
        setKeyboardFocus(true);
        if (didShowCallBack) didShowCallBack();
      },
    );
    const didHideUnsubscribable = Keyboard.addListener(
      'keyboardDidHide',
      () => {
        setKeyboardFocus(false);
        if (didHideCallback) didHideCallback();
      },
    );
    return () => {
      didShowUnsubscribable.remove();
      didHideUnsubscribable.remove();
    };
  }, []);

  return keyboardFocused && Platform.OS === 'ios';
}
