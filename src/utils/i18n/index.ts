import * as RNLocalize from 'react-native-localize';
import i18n, { TranslateOptions } from 'i18n-js';
import memoize from 'lodash/memoize';
import moment from 'moment';

import { I18nManager } from 'react-native';

const translationGetters = {
  en: () => require('#/translations/en.json'),
};

const translations = memoize(
  (key: string, config?: TranslateOptions) => i18n.t(key, config),
  (key: string, config?: TranslateOptions) =>
    config ? key + JSON.stringify(config) : key,
);

const setI18nConfig = () => {
  // fallback if no available language fits
  const fallback = { languageTag: 'en', isRTL: false };

  const { languageTag, isRTL } =
    RNLocalize.findBestAvailableLanguage(Object.keys(translationGetters)) ||
    fallback;

  // clear translation cache
  translations.cache.clear();

  // update layout direction
  I18nManager.forceRTL(isRTL);

  // set i18n-js config
  i18n.translations = { [languageTag]: translationGetters[languageTag]() };
  i18n.locale = languageTag;

  switch (languageTag) {
    default:
    case 'en':
      moment.locale('en');
      break;
  }
};

export { setI18nConfig, translations };
