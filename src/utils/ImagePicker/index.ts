import ImagePicker from 'react-native-image-crop-picker';

interface PhotoOptions {
  width: number;
  height: number;
  cropping: boolean;
  mediaType: 'photo' | 'video' | any | undefined;
  includeBase64: boolean;
  compressImageQuality: number;
}

export const DEFAULT_OPTIONS = {
  width: 335,
  height: 200,
  cropping: false,
  mediaType: 'photo',
  includeBase64: false,
  compressImageQuality: 0.8,
};

export default {
  openCamera: (callback: Function, options: PhotoOptions = DEFAULT_OPTIONS) => {
    ImagePicker.openCamera(options)
      .then(image => {
        const canceled = false;
        callback({ image, canceled });
      })
      .catch(() => {
        const canceled = false;
        callback({ canceled });
      });
  },

  openPicker: (callback: Function, options: PhotoOptions = DEFAULT_OPTIONS) => {
    ImagePicker.openPicker(options)
      .then(image => {
        const canceled = false;
        if (image) callback({ image, canceled });
      })
      .catch(() => {
        const canceled = false;
        callback({ canceled });
      });
  },
};
