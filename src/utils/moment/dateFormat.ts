import moment from 'moment';

export function getLLDateFormat(timeStamp: number): string {
  return moment(timeStamp).format('ll');
}
