import { get } from '~/api/utils/request';
import { UserAddressesListResult } from './userTypes';

export default (): Promise<UserAddressesListResult> =>
  new Promise(async resolve => {
    const result: UserAddressesListResult = {
      error: undefined,
      response: undefined,
    };

    try {
      const response = await get('user/addresses');
      result.response = response;
      resolve(result);
    } catch (error) {
      result.error = error;
      resolve(result);
    }
  });
