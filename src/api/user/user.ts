import { get } from '~/api/utils/request';
import { UserResult } from './userTypes';

export default (): Promise<UserResult> =>
  new Promise(async resolve => {
    const result: UserResult = {
      error: undefined,
      response: undefined,
    };

    try {
      const response = await get('user');
      result.response = response;
      resolve(result);
    } catch (error) {
      result.error = error;
      resolve(result);
    }
  });
