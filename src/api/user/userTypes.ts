import { ErrorResult, ResponseResult } from '~/api/utils/apiTypes';

export interface IUserData {
  firstName: string;
  lastName: string;
  emailAddress: string;
  currency: string;
  mobile: string;
}

//user
export interface UserResponse extends IUserData {
  id: string;
}

export interface UserResult extends ErrorResult {
  response: UserResponse | ResponseResult;
}

//userAddress
export interface UserAddressesListResponse {
  guid: string;
  city: string;
  postCode: string;
  address: string;
}

export interface UserAddressesListResult extends ErrorResult {
  response: UserAddressesListResponse[] | ResponseResult;
}

//addUserAddress
export interface AddUserAddressBody {
  city: string;
  postCode: string;
  address: string;
}

export interface AddUserAddressResponse {
  city: string;
  postCode: string;
  address: string;
}

export interface AddUserAddressResult extends ErrorResult {
  response: AddUserAddressResponse | ResponseResult;
}

//deleteUserAddress
export interface DeleteUserAddressBody {
  guid: string;
}

export interface DeleteUserAddressResponse {
  city: string;
  postCode: string;
  address: string;
}

export interface DeleteUserAddressResult extends ErrorResult {
  response: DeleteUserAddressResponse | ResponseResult;
}

//userUpdate
export interface UserUpdateBody {
  firstName?: string;
  lastName?: string;
  emailAddress?: string;
  currency?: string;
}

export interface UserUpdateResponse {
  status: true;
}

export interface UserUpdateResult extends ErrorResult {
  response: UserUpdateResponse | ResponseResult;
}
