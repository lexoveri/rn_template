import { del } from '~/api/utils/request';
import { DeleteUserAddressResult, DeleteUserAddressBody } from './userTypes';

export default (
  data: DeleteUserAddressBody,
): Promise<DeleteUserAddressResult> =>
  new Promise(async resolve => {
    const result: DeleteUserAddressResult = {
      error: undefined,
      response: undefined,
    };

    try {
      const response = await del('user/addresses', data);
      result.response = response;
      resolve(result);
    } catch (error) {
      result.error = error;
      resolve(result);
    }
  });
