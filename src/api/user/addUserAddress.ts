import { post } from '~/api/utils/request';
import { AddUserAddressResult, AddUserAddressBody } from './userTypes';

export default (data: AddUserAddressBody): Promise<AddUserAddressResult> =>
  new Promise(async resolve => {
    const result: AddUserAddressResult = {
      error: undefined,
      response: undefined,
    };

    try {
      const response = await post('user/addresses', data);
      result.response = response;
      resolve(result);
    } catch (error) {
      result.error = error;
      resolve(result);
    }
  });
