import { put } from '~/api/utils/request';
import { UserUpdateResult, UserUpdateBody } from './userTypes';

export default (body: UserUpdateBody): Promise<UserUpdateResult> =>
  new Promise(async resolve => {
    const result: UserUpdateResult = {
      error: undefined,
      response: undefined,
    };

    try {
      const response = await put('user', body);
      result.response = response;
      resolve(result);
    } catch (error) {
      result.error = error;
      resolve(result);
    }
  });
