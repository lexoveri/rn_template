import user from './user';
import userAddressesList from './userAddressesList';
import addUserAddress from './addUserAddress';
import deleteUserAddress from './deleteUserAddress';
import userUpdate from './userUpdate';

export default {
  user,
  userAddressesList,
  addUserAddress,
  deleteUserAddress,
  userUpdate,
};
