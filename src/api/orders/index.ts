import orders from './orders';
import getOrder from './getOrder';
import createOrder from './createOrder';

export default {
  orders,
  createOrder,
  getOrder,
};
