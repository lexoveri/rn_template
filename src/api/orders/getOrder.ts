import { get } from '~/api/utils/request';
import { GetOrderBody, GetOrderResult } from './ordersTypes';

export default ({ uuid }: GetOrderBody): Promise<GetOrderResult> =>
  new Promise(async resolve => {
    const result: GetOrderResult = {
      error: undefined,
      response: undefined,
    };

    try {
      const response = await get(`orders/${uuid}`);
      result.response = response;
      resolve(result);
    } catch (error) {
      result.error = error;
      resolve(result);
    }
  });
