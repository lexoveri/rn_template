import { post } from '~/api/utils/request';
import { CreateOrderBody, CreateOrderResult } from './ordersTypes';

export default (order: CreateOrderBody): Promise<CreateOrderResult> =>
  new Promise(async resolve => {
    const result: CreateOrderResult = {
      error: undefined,
      response: undefined,
    };

    try {
      const response = await post(`orders`, order);
      result.response = response;
      resolve(result);
    } catch (error) {
      result.error = error;
      resolve(result);
    }
  });
