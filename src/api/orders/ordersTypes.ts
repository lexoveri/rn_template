import { ErrorResult, ResponseResult } from '~/api/utils/apiTypes';

export interface IOrder {
  uuid: string;
  address: string;
  delivery_datetime: string;
  status: string;
}

//getOrders
export interface OrdersResponse {
  uuid: string;
  address: string;
  delivery_datetime: number;
  status: string;
}

export interface OrdersResult extends ErrorResult {
  response: OrdersResponse | ResponseResult;
}

export interface OrdersBody {
  uuid: string;
  page: string;
  limit: string;
}

//getOrder
export interface GetOrderBody {
  uuid: string;
}

export interface GetOrderResponse {
  address: string;
  delivery_datetime: number;
  status: string;
  products: IOrder[];
}

export interface GetOrderResult extends ErrorResult {
  response: GetOrderResponse | ResponseResult;
}

// createOrder
export interface CreateOrderBody {
  address: string; //uuid
  delivery_datetime: number;
  products: [
    {
      uuid: string;
      qty: number;
    },
  ];
}

export interface CreateOrderResponse {
  status: boolean;
}

export interface CreateOrderResult extends ErrorResult {
  response: CreateOrderResponse | ResponseResult;
}
