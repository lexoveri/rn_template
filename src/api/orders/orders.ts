import { get } from '~/api/utils/request';
import { OrdersBody, OrdersResult } from './ordersTypes';

export default (queries: OrdersBody): Promise<OrdersResult> =>
  new Promise(async resolve => {
    const result: OrdersResult = {
      error: undefined,
      response: undefined,
    };

    try {
      const response = await get('orders', queries);
      result.response = response;
      resolve(result);
    } catch (error) {
      result.error = error;
      resolve(result);
    }
  });
