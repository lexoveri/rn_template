import apiConfig from '~/api/utils/apiConfig';
import serializeQuery from '~/api/utils/serializeQuery';
import { store } from '~/index';
import { authAccessTokenToken } from '~/modules/auth/selectors';

const checkStatus = (response) => {
  if (response.status > 210) {
    console.log('API ERROR', response);
    throw response;
  }
  return response;
};

export const request = (
  url: string,
  method: string,
  data?: any,
  query?: any,
  headers: any = {},
) => {
  return new Promise((resolve, reject) => {
    let queryStr = '';
    if (query) {
      queryStr = serializeQuery(query);
    }

    const token = authAccessTokenToken(store.getState());
    if (token) headers['Authorization'] = token;

    const fullUrl = `${apiConfig.apiUrl}/${url}${queryStr}`;
    console.log(`API REQUEST ${method}`, url + queryStr, data);
    fetch(fullUrl, {
      method: method || 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        ...headers,
      },
      // mode: 'cors',
      body: (method !== 'GET' && data && JSON.stringify(data)) || undefined,
    })
      .then((response) => {
        return response.json();
      })
      .then(checkStatus)
      .then((response) => {
        console.log(`API RESPONSE ${method}`, url + queryStr, response);
        return response;
      })
      .then(resolve)
      .catch((error) => {
        throw error;
      })
      .catch(reject);
  });
};

export const get = (url: string, query?: any, headers?: any) => {
  return request(url, 'GET', null, query, headers);
};

export const post = (url: string, data?: any, query?: any, headers?: any) => {
  return request(url, 'POST', data, query, headers);
};

export const put = (url: string, data?: any, query?: any, headers?: any) => {
  return request(url, 'PUT', data, query, headers);
};

export const del = (url: string, data?: any, query?: any) => {
  return request(url, 'DELETE', data, query);
};

export const patch = (url: string, data?: any, query?: any, headers?: any) => {
  return request(url, 'PATCH', data, query, headers);
};
