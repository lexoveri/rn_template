export default function serializeQuery(obj: any, prefix?: any) {
  const queries = {};

  for (let key in obj) {
    if (obj[key].length) {
      queries[key] = obj[key];
    }
  }
  const queriesKeys = Object.keys(queries);
  if (!queriesKeys.length) return '';

  return (
    '?' +
    queriesKeys
      .map(function(key) {
        const stringVal = Array.isArray(queries[key])
          ? queries[key].join(',')
          : encodeURIComponent(queries[key]);
        return encodeURIComponent(key) + '=' + stringVal;
      })
      .join('&')
  );
}
