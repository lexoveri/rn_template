export interface ErrorResult {
  error:
    | {
        status: number;
        code: string;
        message: string;
      }
    | undefined;
}

export type ResponseResult = undefined | any;

export interface SignUpUserData {
  firstName: string;
  lastName: string;
  tz?: string;
  mobile: string;
  emailAddress: string;
}

export interface SignInUserData {
  tz: string;
  deviceToken: string;
}
