import { ErrorResult, ResponseResult } from '~/api/utils/apiTypes';

export interface GetAuthTokenResponse {
  token: string;
}

export interface SingInResponse {
  token: string;
}

export interface SignUpResponse {
  token: string;
  success: boolean;
}

export interface DeleteResponse {
  success: boolean;
}

export interface ResendOTPResponse {
  nextResend: number;
  remaindedResendAttempts: number;
}

export interface SignUpResult extends ErrorResult {
  response: SignUpResponse | ResponseResult;
}

export interface SingInResult extends ErrorResult {
  response: SingInResponse | ResponseResult;
}

export interface DeleteResult extends ErrorResult {
  response: DeleteResponse | ResponseResult;
}

export interface GetAuthTokenResult extends ErrorResult {
  response: GetAuthTokenResponse | ResponseResult;
}

export interface ResendOTPResult extends ErrorResult {
  response: ResendOTPResponse | ResponseResult;
}
