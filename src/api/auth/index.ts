import singIn from './singIn';
import signUp from './signUp';
import deleteUser from './deleteUser';
import getAuthToken from './getAuthToken';
import resendOTP from './resendOTP';

export default {
  singIn,
  signUp,
  deleteUser,
  getAuthToken,
  resendOTP,
};
