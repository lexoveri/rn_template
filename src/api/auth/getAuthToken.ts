import { post } from '~/api/utils/request';
import { GetAuthTokenResult } from './authTypes';

export default (data): Promise<GetAuthTokenResult> =>
  new Promise(async resolve => {
    const result: GetAuthTokenResult = {
      error: undefined,
      response: undefined,
    };

    try {
      const response = await post('auth/auth-token', data);
      result.response = response;
      resolve(result);
    } catch (error) {
      result.error = error;
      resolve(result);
    }
  });
