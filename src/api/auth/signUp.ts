import { post } from '~/api/utils/request';
import { SignUpUserData } from '~/api/utils/apiTypes';
import { SignUpResult } from './authTypes';

export default (userData: SignUpUserData): Promise<SignUpResult> =>
  new Promise(async resolve => {
    const result: SignUpResult = {
      error: undefined,
      response: undefined,
    };

    try {
      const response = await post('auth/register', userData);
      result.response = response;
      resolve(result);
    } catch (error) {
      result.error = error;
      resolve(result);
    }
  });
