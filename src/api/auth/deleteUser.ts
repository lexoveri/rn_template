import { del } from '~/api/utils/request';
import { DeleteResult } from './authTypes';

export default (mobile: string): Promise<DeleteResult> =>
  new Promise(async resolve => {
    const result: DeleteResult = {
      error: undefined,
      response: undefined,
    };

    try {
      const response = await del('auth/delete_user', { mobile });
      result.response = response;
      resolve(result);
    } catch (error) {
      result.error = error;
      resolve(result);
    }
  });
