import { get } from '~/api/utils/request';
import { ResendOTPResult } from './authTypes';

export default (): Promise<ResendOTPResult> =>
  new Promise(async resolve => {
    const result: ResendOTPResult = {
      error: undefined,
      response: undefined,
    };

    try {
      const response = await get('auth-token/resend-otp');
      result.response = response;
      resolve(result);
    } catch (error) {
      result.error = error;
      resolve(result);
    }
  });
