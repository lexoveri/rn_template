import { post } from '~/api/utils/request';
import { SingInResult } from './authTypes';
import { SignInUserData } from '~/api/utils/apiTypes';

export default (signInData: SignInUserData): Promise<SingInResult> =>
  new Promise(async resolve => {
    const result: SingInResult = {
      error: undefined,
      response: undefined,
    };

    try {
      const response = await post('auth/login', signInData);
      result.response = response;
      resolve(result);
    } catch (error) {
      result.error = error;
      resolve(result);
    }
  });
