import auth from './auth';
import user from './user';
import orders from './orders';

export default {
  auth,
  user,
  orders,
};
