import React, { Component } from 'react';
import { UIManager } from 'react-native';
import SplashScreen from 'react-native-splash-screen';

import RootNavigation from '~/navigation';

import { STATE_REHYDRATION } from '~/modules/actions';
import { setI18nConfig } from '~/utils/i18n';
import { Store } from 'redux';
import { isMountedRef } from '~/navigation/utils/NavigationService';

setI18nConfig();

UIManager.setLayoutAnimationEnabledExperimental &&
  UIManager.setLayoutAnimationEnabledExperimental(true);

interface PropsType {
  globalStore: Store;
}

export default class App extends Component<PropsType, {}> {
  navigationRef = React.createRef();

  componentDidMount() {
    SplashScreen.hide();

    isMountedRef.current = true;

    this.props.globalStore.dispatch(STATE_REHYDRATION.DONE.create());
  }

  componentDidCatch(error: Error, errorInfo: React.ErrorInfo): void {
    console.log({
      error,
      errorInfo,
    });
  }

  componentWillUnmount(): void {
    isMountedRef.current = false;
  }

  render() {
    return <RootNavigation />;
  }
}
