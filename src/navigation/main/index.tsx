import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import {
  createStackNavigator,
  StackNavigationOptions,
} from '@react-navigation/stack';
import { StackNavigationConfig } from '@react-navigation/stack/lib/typescript/src/types';

import AuthStack from './auth';
import TabBarStack from './tabs';

import { RootStackType } from '~/navigation/main/types';
import { isUserLoggedInSelector } from '~/modules/user/selectors';

const Stack = createStackNavigator<RootStackType>();

const mainStackOptions: StackNavigationConfig = {
  headerMode: 'none',
  mode: 'modal',
};

const screenOptions: StackNavigationOptions = {
  cardStyle: { backgroundColor: 'transparent' },
};

const MainStack = ({ isUserLoggedIn }) => {
  const [isLogged, setStatus] = useState(false);
  useEffect(() => {
    setStatus(isUserLoggedIn);
  }, []);

  return (
    <Stack.Navigator
      screenOptions={screenOptions}
      {...mainStackOptions}
      initialRouteName={'AuthStack'}
    >
      <Stack.Screen name="TabBarStack" component={TabBarStack} />
    </Stack.Navigator>
  );
};

const mapStateToProps = state => ({
  isUserLoggedIn: isUserLoggedInSelector(state),
});

export default connect(mapStateToProps)(MainStack);
