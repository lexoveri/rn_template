import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import ProfileStack from './profileStack';

import PageTab1 from '~/ui/screens/PageTab1';
import PageTab2 from '~/ui/screens/PageTab2';

import { TabStack, TabBarNavigator, ChatStackList } from './types';

import { BottomTabBar } from '~/ui/components';
import { createStackNavigator } from '@react-navigation/stack';
import { EditProfile } from '~/ui/screens/profile';
import { getScreenHeader } from '~/navigation/utils/navigationOptions';

const Tab = createBottomTabNavigator<TabBarNavigator>();

function MainTabBarNavigator() {
  return (
    <Tab.Navigator lazy={false} tabBar={(props) => <BottomTabBar {...props} />}>
      <Tab.Screen name="ProfileStack" component={ProfileStack} />
      <Tab.Screen name="PageTab1" component={PageTab1} />
      <Tab.Screen name="PageTab2" component={PageTab2} />
    </Tab.Navigator>
  );
}

const ScreenStackNav = createStackNavigator<ChatStackList>();
const ScreenStack = () => (
  <ScreenStackNav.Navigator headerMode={'none'} initialRouteName={'Chat'}>
    <ScreenStackNav.Screen
      options={getScreenHeader()}
      name="Edit"
      component={EditProfile}
    />
  </ScreenStackNav.Navigator>
);

const Stack = createStackNavigator<TabStack>();
export default () => (
  <Stack.Navigator headerMode={'none'} initialRouteName={'TabBar'}>
    <Stack.Screen name="TabBar" component={MainTabBarNavigator} />
    <Stack.Screen name="ScreenStack" component={ScreenStack} />
  </Stack.Navigator>
);
