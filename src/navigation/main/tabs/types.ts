import { StackNavigationProp } from '@react-navigation/stack';

export type TabStack = {
  TabBar: undefined;
  EditProfile: undefined;
  ScreenStack: undefined;
};

// ======== Profile stack =========

export type ProfileStackList = {
  ProfilePage: undefined;
  MyPrescriptions: undefined;
  OrderPage: undefined;
  Product: undefined;
};

export type ProfileInfoScreenNavigationProp = StackNavigationProp<
  ProfileStackList,
  'ProfilePage'
>;

export type TabBarNavigator = {
  PageTab1: undefined;
  PageTab2: undefined;
  ProfileStack: ProfileStackList;
};
