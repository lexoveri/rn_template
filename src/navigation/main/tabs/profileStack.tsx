import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import { ProfileStackList } from './types';
import { ProfilePage } from '~/ui/screens/profile';
import { getScreenHeader } from '~/navigation/utils/navigationOptions';

const Stack = createStackNavigator<ProfileStackList>();
export default () => (
  <Stack.Navigator headerMode="screen" initialRouteName={'ProfilePage'}>
    <Stack.Screen
      name="ProfilePage"
      component={ProfilePage}
      options={getScreenHeader({ hideHeader: true })}
    />
  </Stack.Navigator>
);
