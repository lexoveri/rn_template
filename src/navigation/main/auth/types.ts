import { RouteProp } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';

export type SignInStackParamList = {
  SignIn: undefined;
  SignInEmailSent: undefined;
};

export type AuthStackParamList = {
  SignInStack: SignInStackParamList;
  Welcome: undefined;
  SignUpStack: undefined;
};

export type SignInEmailScreenNavigationProp = StackNavigationProp<
  SignInStackParamList,
  'SignInEmail'
>;
export type SignInEmailScreenRouteProp = RouteProp<
  SignInStackParamList,
  'SignInEmail'
>;

export type SignInEmailSentScreenRouteProp = RouteProp<
  SignInStackParamList,
  'SignInEmailSent'
>;

export type SignInEmailSentScreenNavigationProp = StackNavigationProp<
  SignInStackParamList,
  'SignInEmailSent'
>;

export type SignUpStackList = {
  AddUserInfo: undefined;
  RegistrationCompleted: undefined;
};

export type AddUserInfoScreenNavigationProp = StackNavigationProp<
  SignUpStackList,
  'AddUserInfo'
>;
