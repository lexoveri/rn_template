import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import { SignInEmail, Welcome, SignUp } from '~/ui/screens/auth';

import {
  SignInStackParamList,
  AuthStackParamList,
  SignUpStackList,
} from './types';
import { getScreenHeader } from '~/navigation/utils/navigationOptions';
const SignInStack = createStackNavigator<SignInStackParamList>();

function signInStack() {
  return (
    <SignInStack.Navigator headerMode={'screen'}>
      <SignInStack.Screen
        options={getScreenHeader({ hideBackButton: true })}
        name="SignIn"
        component={SignInEmail}
      />
    </SignInStack.Navigator>
  );
}

const SignUpStack = createStackNavigator<SignUpStackList>();

const signUpStack = () => (
  <SignUpStack.Navigator headerMode={'none'} initialRouteName={'AddUserInfo'}>
    <SignUpStack.Screen name="AddUserInfo" component={SignUp} />
  </SignUpStack.Navigator>
);

const AuthStack = createStackNavigator<AuthStackParamList>();

export default () => (
  <AuthStack.Navigator headerMode="none" initialRouteName="Welcome">
    <AuthStack.Screen name="SignInStack" component={signInStack} />
    <AuthStack.Screen name="SignUpStack" component={signUpStack} />
    <AuthStack.Screen name="Welcome" component={Welcome} />
  </AuthStack.Navigator>
);
