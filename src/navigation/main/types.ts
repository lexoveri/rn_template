import * as AuthStackTypes from '~/navigation/main/auth/types';
import { TabBarNavigator } from '~/navigation/main/tabs/types';

export type RootStackType = {
  InitialRouter: undefined;
  AuthStack: AuthStackTypes.AuthStackParamList;
  TabBarStack: TabBarNavigator;
};

export { AuthStackTypes };
