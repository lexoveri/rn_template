import { View } from 'react-native';
import Header from '~/ui/components/Header';
import React from 'react';
import { colors } from '~/ui/style';

export interface IDefaultOptions {
  hideBackButton?: boolean;
  withoutAnimation?: boolean;
  hideHeader?: boolean;
  steps?: number;
  activeStep?: number;
  gestureEnabled?: boolean;
}

const defaultOptions: IDefaultOptions = {
  hideBackButton: false,
  withoutAnimation: false,
  hideHeader: false,
  steps: 0,
  activeStep: 0,
  gestureEnabled: true,
};

export const getScreenHeader = (options: IDefaultOptions = defaultOptions) => {
  return {
    header: ({ navigation }) => {
      if (options.hideHeader) return <View />;
      return (
        <Header
          navigation={navigation}
          onPress={navigation.goBack}
          options={options}
        />
      );
    },
    gestureEnabled: options.gestureEnabled,
    cardStyle: { backgroundColor: colors.mainBG },
  };
};
