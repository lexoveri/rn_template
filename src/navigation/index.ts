import NavigationService from './utils/NavigationService';
import MainStackNavigator from './main';

const TopNavigator = MainStackNavigator;

export { NavigationService };
export default TopNavigator;
