import React from 'react';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { NavigationContainer } from '@react-navigation/native';
import 'react-native-gesture-handler';

import App from './App';

import configStore from './configStore';
import { navigationRef } from '~/navigation/utils/NavigationService';
export const { store, persistor } = configStore();

console.disableYellowBox = true;
console.log(true)

export default () => {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <NavigationContainer ref={navigationRef}>
          <App globalStore={store} />
        </NavigationContainer>
      </PersistGate>
    </Provider>
  );
};
