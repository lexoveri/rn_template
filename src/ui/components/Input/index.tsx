import React, { Component } from 'react';
import {
  StyleSheet,
  TextInput,
  View,
  Animated,
  ViewStyle,
  TextInputProps,
  TouchableOpacity,
} from 'react-native';
import { colors, shadowParams } from '../../style';
import { Text } from '~/ui/components';
import { borderRadius } from '~/ui/style';

interface CustomInputProps extends TextInputProps {
  nativeID?: string;
  inputType?: string;
  label: string;
  style?: ViewStyle;
  inputWrapperStyle?: ViewStyle;
  handleRef?: any;
  error?: boolean | string;
  errorMessage?: string;
  labelTopPosition?: number;
  labelStyles?: ViewStyle;
  value: string;
  onInputPress: () => void;
  blur: (val?: any) => any;
}

interface IState {
  isFocused: boolean;
}

export default class Input extends Component<CustomInputProps, IState> {
  state = {
    isFocused: false,
  };
  private _animatedIsFocused: Animated.Value;

  // eslint-disable-next-line react/no-deprecated
  componentWillMount() {
    this._animatedIsFocused = new Animated.Value(
      this.props.value === '' ? 0 : 1,
    );
  }

  handleFocus = () => this.setState({ isFocused: true });
  handleBlur = value => {
    // console.log(value)
    if (this.props.blur) this.props.blur(value);
    this.setState({ isFocused: false });
  };

  componentDidUpdate() {
    Animated.timing(this._animatedIsFocused, {
      toValue: this.state.isFocused || this.props.value !== '' ? 1 : 0,
      duration: 90,
    }).start();
  }

  render() {
    const {
      nativeID,
      inputType,
      handleRef,
      style,
      label,
      error,
      errorMessage,
      inputWrapperStyle,
      labelTopPosition,
      labelStyles,
      onInputPress,
      ...props
    } = this.props;

    const inputStyles = [styles.input, style, error && styles.error];

    const keyboardType = inputType || 'default';

    const labelTop = labelTopPosition || 19;

    const labelStyle = {
      ...styles.label,
      top: this._animatedIsFocused.interpolate({
        inputRange: [0, 1],
        outputRange: [labelTop, 7],
      }),
      fontSize: this._animatedIsFocused.interpolate({
        inputRange: [0, 1],
        outputRange: [18, 11],
      }),
    };

    const shadow = this.state.isFocused ? { ...shadowParams } : {};

    return (
      <View style={[styles.inputWrapper, shadow, inputWrapperStyle]}>
        <Animated.Text style={labelStyle}>{label}</Animated.Text>
        <TextInput
          ref={o => {
            if (handleRef) {
              handleRef(o);
            }
          }}
          style={inputStyles}
          autoComplete="off"
          autoCapitalize="none"
          autoCorrect={false}
          keyboardType={keyboardType}
          selectionColor="#2C5BB7"
          numberOfLines={1}
          underlineColorAndroid={'transparent'}
          onFocus={this.handleFocus}
          onBlur={this.handleBlur}
          blurOnSubmit
          {...props}
        />
        {error ? (
          <View style={styles.errorMessage}>
            <Text color="error">{errorMessage}</Text>
          </View>
        ) : null}

        {onInputPress ? (
          <TouchableOpacity style={styles.touchBlock} onPress={onInputPress} />
        ) : null}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  input: {
    margin: 0,
    width: '100%',
    fontSize: 15,
    lineHeight: 18,
    height: 18,
    color: colors.primary,
    padding: 0,
  },
  inputWrapper: {
    backgroundColor: colors.white,
    flexDirection: 'row',
    paddingHorizontal: 16,
    paddingTop: 25,
    borderRadius,
    height: 56,
    position: 'relative',
    marginVertical: 7.5,
  },
  label: {
    position: 'absolute',
    height: 18,
    left: 16,
    // fontFamily: 'Rubik',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 15,
    lineHeight: 18,
    letterSpacing: -0.185294,
    color: colors.gray,
  },
  error: {
    color: colors.error,
  },
  errorMessage: {
    position: 'absolute',
    bottom: -24,
    paddingLeft: 16,
    left: 0,
  },
  touchBlock: {
    flex: 1,
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    position: 'absolute',
  },
});
