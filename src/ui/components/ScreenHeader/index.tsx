import React from 'react';
import { View, StyleSheet, ViewStyle } from 'react-native';
import Text, { TypographyProps } from '../Text';

const styles = StyleSheet.create({
  wrapper: {
    width: '100%',
    flexDirection: 'column',
    alignContent: 'flex-start',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  descriptionWrapper: {
    width: '100%',
  },
  descriptionText: {
    marginTop: 16,
  },
});

interface ScreenHeaderProps {
  title: string;
  description?: string;
  titleProps?: TypographyProps;
  descriptionProps?: TypographyProps;
  style?: ViewStyle;
}

export default class ScreenHeader extends React.Component<ScreenHeaderProps> {
  render() {
    const {
      title,
      description,
      style,
      titleProps,
      descriptionProps,
    } = this.props;
    return (
      <View style={[styles.wrapper, style]}>
        {title ? (
          <Text height={38} h1 primary {...titleProps}>
            {title}
          </Text>
        ) : null}
        {description ? (
          <View style={styles.descriptionWrapper}>
            <Text
              style={styles.descriptionText}
              size={17}
              tertiary
              {...descriptionProps}
            >
              {description}
            </Text>
          </View>
        ) : null}
      </View>
    );
  }
}
