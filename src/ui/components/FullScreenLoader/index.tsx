import React, { ReactChildren } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Modal,
  ActivityIndicator,
  TextStyle,
  ViewStyle,
} from 'react-native';

const transparent = 'transparent';
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: transparent,
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
  },
  background: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textContainer: {
    flex: 1,
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
  },
  textContent: {
    top: 80,
    height: 50,
    fontSize: 20,
    fontWeight: 'bold',
  },
  activityIndicator: {
    flex: 1,
  },
});

// const ANIMATION = ['none', 'slide', 'fade'];
// const SIZES = ['small', 'normal', 'large'];

type IProps = {
  cancelable: boolean;
  color: string;
  animation: any;
  overlayColor: string;
  size: any;
  textContent: string;
  textStyle: TextStyle;
  visible: boolean;
  indicatorStyle: ViewStyle;
  customIndicator: ReactChildren;
  children: ReactChildren;
  spinnerKey: string;
};

type IState = {
  visible: boolean;
  textContent: string;
};

export default class Spinner extends React.PureComponent<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      visible: this.props.visible,
      textContent: this.props.textContent,
    };
  }

  close() {
    this.setState({ visible: false });
  }

  static getDerivedStateFromProps(props: IProps, state: IState) {
    const newState = { visible: false, textContent: '' };
    if (state.visible !== props.visible) {
      newState.visible = props.visible;
    }
    if (state.textContent !== props.textContent) {
      newState.textContent = props.textContent;
    }
    return newState;
  }

  _handleOnRequestClose() {
    if (this.props.cancelable) {
      this.close();
    }
  }

  _renderDefaultContent() {
    return (
      <View style={styles.background}>
        {this.props.customIndicator ? (
          this.props.customIndicator
        ) : (
          <ActivityIndicator
            color={this.props.color}
            size={this.props.size}
            style={[styles.activityIndicator, { ...this.props.indicatorStyle }]}
          />
        )}
        <View style={[styles.textContainer, { ...this.props.indicatorStyle }]}>
          <Text style={[styles.textContent, this.props.textStyle]}>
            {this.state.textContent}
          </Text>
        </View>
      </View>
    );
  }

  _renderSpinner() {
    if (!this.state.visible) {
      return null;
    }

    const spinner = (
      <View
        style={[styles.container, { backgroundColor: this.props.overlayColor }]}
        key={
          this.props.spinnerKey
            ? this.props.spinnerKey
            : `spinner_${Date.now()}`
        }
      >
        {this.props.children
          ? this.props.children
          : this._renderDefaultContent()}
      </View>
    );

    return (
      <Modal
        animationType={this.props.animation}
        onRequestClose={() => this._handleOnRequestClose()}
        supportedOrientations={['landscape', 'portrait']}
        transparent
        visible={this.state.visible}
      >
        {spinner}
      </Modal>
    );
  }

  render() {
    return this._renderSpinner();
  }
}
