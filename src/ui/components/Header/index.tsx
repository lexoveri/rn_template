import React from 'react';
import { StyleSheet, View } from 'react-native';
import { colors, sizes } from '~/ui/style';
import { IDefaultOptions } from '~/navigation/utils/navigationOptions';
import { useSafeArea } from 'react-native-safe-area-context';

interface Props {
  onPress: () => void;
  options: IDefaultOptions;
}

export default function Header({
  onPress,
  navigation,
  options: { hideBackButton, withoutAnimation, steps, activeStep },
}: Props) {
  const insets = useSafeArea();

  const stepsComponent = [];

  if (steps) {
    for (let i = 0; i < steps; i++) {
      stepsComponent.push(
        <View
          key={`${i}-header-step`}
          style={[styles.step, i === activeStep && styles.activeStep]}
        />,
      );
    }
  }

  function goBack() {
    console.log(navigation.canGoBack());
    return onPress ? onPress() : navigation.goBack();
  }

  return (
    <View
      style={[
        [styles.headerWrapper, { height: sizes.headerHeight + insets.top }],
      ]}
    >
      {hideBackButton ? (
        <View style={styles.noButton} />
      ) : (
        <View style={styles.headerContent}>
          <View style={styles.stepsBlock}>{stepsComponent}</View>

          <View style={{ width: 42 }} />
        </View>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  headerWrapper: {
    height: 100,
    paddingHorizontal: 16,
    backgroundColor: colors.mainBG,
    justifyContent: 'flex-end',
    alignItems: 'flex-start',
    alignContent: 'flex-end',
  },
  noButton: {
    height: 50,
  },
  headerContent: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginBottom: 10,
  },
  stepsBlock: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    flex: 1,
    marginHorizontal: 50,
  },
  step: {
    flex: 1,
    height: 4,
    backgroundColor: '#CACDDA',
    marginHorizontal: 3,
    borderRadius: 2,
  },
  activeStep: {
    backgroundColor: '#516182',
  },
});
