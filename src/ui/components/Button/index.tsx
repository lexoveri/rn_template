import React, { Component } from 'react';
import {
  ActivityIndicator,
  StyleSheet,
  TouchableOpacity,
  TouchableOpacityProps,
  View,
  ViewStyle,
} from 'react-native';

import { translations } from '~/utils/i18n';
import { colors, borderRadius } from '~/ui/style';
import { Text } from '~/ui/components';
import { TypographyProps } from '~/ui/components/Text';

interface Props extends TouchableOpacityProps {
  onPress: any;
  onLongPress?: any;
  buttonText: string;
  iconColor?: string;
  style?: ViewStyle;
  disable?: boolean;
  blueFb?: boolean;
  isLoading?: boolean;
  white?: boolean;
  transparent?: boolean;
  icon?: string;
  textProps?: TypographyProps;
}

export default class Button extends Component<Props> {
  render() {
    const {
      buttonText,
      disable,
      style,
      onPress,
      onLongPress,
      white,
      transparent,
      blueFb,
      textProps,
      isLoading,
      ...props
    } = this.props;

    const whiteButton = white;

    const isDisable = typeof disable === 'undefined' ? false : disable;
    const isLoad = typeof isLoading === 'undefined' ? false : isLoading;
    const buttonColor =
      (isDisable && { backgroundColor: colors.buttonDisabled }) ||
      (whiteButton && styles.white) ||
      (transparent && styles.transparent) ||
      (blueFb && styles.blueFb);

    return (
      <View style={[styles.wrapper, style]}>
        <TouchableOpacity
          activeOpacity={isDisable ? 1 : 0.6}
          style={[styles.button, buttonColor]}
          onPress={isDisable ? undefined : onPress}
          onLongPress={isDisable ? undefined : onLongPress}
          {...props}
        >
          <View style={styles.contentWrapper}>
            {isLoad ? (
              <ActivityIndicator color={colors.accent} size={20} />
            ) : (
              <Text
                transform="uppercase"
                white={!whiteButton && !transparent}
                secondary={whiteButton || transparent}
                medium
                {...textProps}
              >
                {buttonText || translations('next')}
              </Text>
            )}
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    marginVertical: 7.5,
    height: 52,
  },
  button: {
    backgroundColor: colors.buttonActive,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius,
    width: '100%',
    flexDirection: 'row',
    height: '100%',
  },
  white: {
    backgroundColor: colors.white,
  },
  transparent: {
    backgroundColor: 'transparent',
  },
  blueFb: {
    backgroundColor: '#3A50B5',
  },
  contentWrapper: {
    flexDirection: 'row',
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
  },
  iconWrapper: {
    width: 1,
    height: 1,
    position: 'absolute',
    left: -14,
    alignContent: 'flex-end',
    alignItems: 'flex-end',
    justifyContent: 'center',
  },
});
