import React from 'react';
import { View, TouchableOpacity, StyleSheet, Image } from 'react-native';
import { borderRadius, colors } from '~/ui/style';
import { useSafeArea } from 'react-native-safe-area-context';

import { Text } from '~/ui/components';

import { BottomTabBarProps } from '@react-navigation/bottom-tabs';

export default function BottomTabBar({
  state,
  descriptors,
  navigation,
}: BottomTabBarProps) {
  const insets = useSafeArea();

  return (
    <View style={{ paddingBottom: insets.bottom, backgroundColor: 'white' }}>
      <View style={{ backgroundColor: colors.mainBG }}>
        <View style={styles.container}>
          {state.routes.map((route, index) => {
            const { options } = descriptors[route.key];

            const isFocused = state.index === index;

            const onPress = () => {
              const event = navigation.emit({
                type: 'tabPress',
                target: route.key,
              });

              if (!isFocused && !event.defaultPrevented) {
                navigation.navigate(route.name);
              }
            };

            const onLongPress = () => {
              navigation.emit({
                type: 'tabLongPress',
                target: route.key,
              });
            };

            return (
              <View style={styles.buttonWrapper}>
                <TouchableOpacity
                  accessibilityRole="button"
                  accessibilityStates={isFocused ? ['selected'] : []}
                  accessibilityLabel={options.tabBarAccessibilityLabel}
                  testID={options.tabBarTestID}
                  onPress={onPress}
                  onLongPress={onLongPress}
                  style={styles.iconWrapper}
                  activeOpacity={1}
                  key={`BottomTabBar-index-${index}`}
                >
                  <Text
                    color={
                      isFocused ? colors.activeTabIcon : colors.disableTabIcon
                    }
                  >
                    {route.name}
                  </Text>
                </TouchableOpacity>
              </View>
            );
          })}
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingVertical: 11,
    backgroundColor: colors.white,
    borderTopRightRadius: borderRadius,
    borderTopLeftRadius: borderRadius,
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  iconWrapper: {
    position: 'relative',
    paddingHorizontal: 5,
    paddingVertical: 5,
  },
  quantityWrapper: {
    position: 'absolute',
    width: 16,
    height: 16,
    backgroundColor: colors.red,
    borderRadius: 8,
    top: -2,
    right: -5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonWrapper: {
    flex: 1,
    alignItems: 'center',
  },
});
