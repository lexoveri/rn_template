import { connect } from 'react-redux';
import BottomTabBar from './BottomTabBar';

function mapStateToProps(state) {
  return {};
}

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(BottomTabBar);
