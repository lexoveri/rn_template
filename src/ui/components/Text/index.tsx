import React, { Component, ReactChildren, ReactText } from 'react';
import { Text, StyleSheet, ViewStyle, TextProps } from 'react-native';

import { fonts, colors, sizes } from '~/ui/style';

export interface TypographyProps extends TextProps {
  h1?: boolean;
  h2?: boolean;
  h3?: boolean;
  title?: string;
  body?: boolean;
  caption?: boolean;
  small?: boolean;
  size?: number;
  transform?: 'uppercase' | 'lowercase' | 'capitalize';
  align?: 'left' | 'center' | 'right';
  // styling
  regular?: boolean;
  bold?: boolean;
  semibold?: boolean;
  medium?: boolean;
  weight?: boolean;
  light?: boolean;
  center?: boolean;
  right?: boolean;
  error?: boolean;
  spacing?: number; // letter-spacing
  height?: number; // line-height
  // colors
  color?:
    | 'color'
    | 'accent'
    | 'primary'
    | 'secondary'
    | 'tertiary'
    | 'black'
    | 'white'
    | 'gray'
    | 'darkGray'
    | 'error';
  accent?: boolean;
  primary?: boolean;
  secondary?: boolean;
  tertiary?: boolean;
  black?: boolean;
  white?: boolean;
  gray?: boolean;
  darkGray?: boolean;
}

export default class Typography extends Component<TypographyProps> {
  render() {
    const {
      h1,
      h2,
      h3,
      title,
      body,
      caption,
      small,
      size,
      transform,
      align,
      error,
      // styling
      regular,
      bold,
      semibold,
      medium,
      weight,
      light,
      center,
      right,
      spacing, // letter-spacing
      height, // line-height
      // colors
      color,
      accent,
      primary,
      secondary,
      tertiary,
      black,
      white,
      gray,
      darkGray,
      style,
      children,
      ...props
    } = this.props;

    const textStyles = [
      styles.text,
      h1 && styles.h1,
      h2 && styles.h2,
      h3 && styles.h3,
      title && styles.title,
      body && styles.body,
      caption && styles.caption,
      size && { fontSize: size },
      transform && { textTransform: transform },
      align && { textAlign: align },
      height && { lineHeight: height },
      spacing && { letterSpacing: spacing },
      weight && { fontWeight: weight },
      regular && styles.regular,
      bold && styles.bold,
      semibold && styles.semibold,
      small && styles.small,
      medium && styles.medium,
      light && styles.light,
      center && styles.center,
      right && styles.right,
      color && styles[color],
      color && !styles[color] && { color },
      // color shortcuts
      accent && styles.accent,
      primary && styles.primary,
      secondary && styles.secondary,
      tertiary && styles.tertiary,
      black && styles.black,
      error && styles.error,
      white && styles.white,
      gray && styles.gray,
      darkGray && styles.darkGray,
      style, // rewrite predefined styles
    ];

    return (
      <Text style={textStyles} {...props}>
        {children}
      </Text>
    );
  }
}

const styles = StyleSheet.create({
  // default style
  text: {
    // fontFamily: 'Rubik-Regular',
    fontSize: sizes.font,
    color: colors.black,
  },
  // variations
  regular: {
    // fontFamily: 'Rubik-Regular',
    fontWeight: 'normal',
  },
  bold: {
    // fontFamily: 'Rubik-Bold',
    fontWeight: 'bold',
  },
  semibold: {
    // fontWeight: '500',
  },
  medium: {
    // fontFamily: 'Rubik-Medium',
    fontWeight: '400',
  },
  light: {
    // fontFamily: 'Rubik-Light',
    fontWeight: '200',
  },
  // position
  center: { textAlign: 'center' },
  right: { textAlign: 'right' },
  // colors
  accent: { color: colors.accent },
  primary: { color: colors.primary },
  secondary: { color: colors.secondary },
  tertiary: { color: colors.tertiary },
  black: { color: colors.black },
  white: { color: colors.white },
  gray: { color: colors.gray },
  darkGray: { color: colors.darkGray },
  error: { color: colors.error },
  // fonts
  h1: fonts.h1,
  h2: fonts.h2,
  h3: fonts.h3,
  title: fonts.title,
  body: fonts.body,
  caption: fonts.caption,
  small: fonts.small,
});
