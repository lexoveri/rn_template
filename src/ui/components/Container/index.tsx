import React from 'react';
import { StyleSheet, View, ViewStyle } from 'react-native';
import { colors, sizes } from '../../style';

const styles = StyleSheet.create({
  wrapper: {
    backgroundColor: colors.mainBG,
    flex: 1,
    paddingHorizontal: sizes.containerPaddingH,
  },
});

interface IProps {
  style?: ViewStyle;
  children?: any;
}

export default function Container({ children, style }: IProps) {
  return <View style={[styles.wrapper, style]}>{children}</View>;
}
