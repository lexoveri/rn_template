import React from 'react';
import { View, ViewProps, ViewStyle } from 'react-native';

interface FlexViewProps extends ViewProps {
  flex?: number | undefined;
  align?: 'flex-start' | 'flex-end' | 'center' | 'stretch' | 'baseline';
  justify?:
    | 'space-between'
    | 'space-around'
    | 'center'
    | 'flex-start'
    | 'flex-end';
  dir?: 'row' | 'column' | 'row-reverse' | 'column-reverse';
  style?: ViewStyle;
  wrap?: string | any;
  children: any;
}

export default function FlexView({
  flex,
  align,
  justify,
  dir,
  children,
  style,
  wrap,
  ...rest
}: FlexViewProps) {
  const flexStyles = [
    { flex: flex ? flex : 1 },
    { alignItems: align ? align : 'center' },
    { justifyContent: justify ? justify : 'center' },
    dir && { flexDirection: dir },
    wrap && { flexWrap: wrap },
  ];

  return (
    <View {...rest} style={flexStyles}>
      {children}
    </View>
  );
}
