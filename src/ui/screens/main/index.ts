import { connect } from 'react-redux';
import Welcome from './Welcome';

function mapStateToProps(state) {
  return {};
}

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(Welcome);
