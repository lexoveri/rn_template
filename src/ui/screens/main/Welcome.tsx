import React from 'react';
import { StyleSheet } from 'react-native';

import { Container, Text, FlexView } from '~/ui/components';

interface WelcomeProps {
  navigation: undefined;
}

export default function({ navigation }: WelcomeProps) {
  return (
    <Container>
      <FlexView>
        <Text>Hi</Text>
      </FlexView>
    </Container>
  );
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
  },
});
