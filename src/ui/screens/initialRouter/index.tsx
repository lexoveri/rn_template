import React, { useEffect } from 'react';
import { useNavigation } from '@react-navigation/core';
import { connect } from 'react-redux';
import { isUserLoggedInSelector } from '~/modules/user/selectors';

interface InitialRouterProps {
  isUserLoggedIn: boolean;
}

function InitialRouter({ isUserLoggedIn }: InitialRouterProps) {
  const navigation = useNavigation();

  useEffect(() => {
    if (isUserLoggedIn) {
      navigation.navigate('TabBarStack');
    } else {
      navigation.navigate('AuthStack');
    }
  }, []);

  return null;
}

function mapStateToProps(state) {
  return {
    isUserLoggedIn: isUserLoggedInSelector(state),
  };
}

export default connect(mapStateToProps)(InitialRouter);
