import React, { useState, useRef } from 'react';
import ActionSheet from 'react-native-actionsheet';
import { StyleSheet, View, ScrollView } from 'react-native';
import { Formik } from 'formik';
import { translations } from '~/utils/i18n';
import ImagePicker from '~/utils/ImagePicker';

import { Button, ScreenHeader, Input } from '~/ui/components';

import { colors, sizes } from '~/ui/style';
import { SignUpUserData } from '~/api/utils/apiTypes';
import { validateEmail } from '~/utils/string';

interface Props {
  createUser: (data: SignUpUserData) => any;
  isLoading: boolean;
}
export default function SignUp({ createUser, isLoading }: Props) {
  const scrollViewRef = useRef();

  const actionSheetRef = useRef(null);

  function _setPhoto({ image, canceled }) {
    if (!canceled) {
      console.log({
        imageSource: { uri: image.path },
      });
    }
  }

  function _actionSheetPress(key: number) {
    switch (key) {
      case 0:
        ImagePicker.openPicker(_setPhoto);
        break;
      case 1:
        ImagePicker.openCamera(_setPhoto);
    }
  }

  function _addPhotoHandler() {
    if (actionSheetRef.current) {
      actionSheetRef.current.show();
    }
  }

  function validate(values) {
    const errors = {};

    if (!values.displayName || values.displayName.trim().length === 0) {
      errors.displayName = true;
    }

    if (!validateEmail(values.email)) {
      errors.email = true;
    }

    return errors;
  }

  function submit(values, action) {
    console.log(values);
    createUser(values);
  }

  return (
    <View style={styles.wrapper}>
      <ScrollView
        showsHorizontalScrollIndicator={false}
        ref={scrollViewRef}
        onContentSizeChange={() => scrollViewRef.current.scrollToEnd()}
        contentContainerStyle={[styles.swContentStyle]}
      >
        <View style={styles.content}>
          <ScreenHeader
            title={translations('addUserInfoScreen.screenTitle')}
            description={translations('addUserInfoScreen.screenDescription')}
            style={{ marginBottom: 32 }}
          />

          <Formik
            enableReinitialize
            initialValues={{
              displayName: '',
              password: '123456',
              email: 'test@test.com',
              phoneNumber: '',
            }}
            // validate={validate}
            onSubmit={submit}
            validateOnBlur
            validateOnChange={false}
          >
            {props => (
              <>
                <Input
                  label={translations('displayName')}
                  value={props.values.displayName}
                  onChangeText={props.handleChange('displayName')}
                  error={props.errors.displayName}
                  blur={props.handleBlur('displayName')}
                />

                <Input
                  label={translations('phone')}
                  value={props.values.phoneNumber}
                  onChangeText={props.handleChange('phoneNumber')}
                  error={props.errors.phoneNumber}
                  blur={props.handleBlur('phoneNumber')}
                />

                <Input
                  label={translations('email')}
                  value={props.values.email}
                  onChangeText={props.handleChange('email')}
                  error={props.errors.email}
                  blur={props.handleBlur('email')}
                />

                <Input
                  label={translations('password')}
                  value={props.values.password}
                  onChangeText={props.handleChange('password')}
                  blur={props.handleBlur('password')}
                  secureTextEn
                />

                <Button
                  style={styles.button}
                  onPress={props.handleSubmit}
                  buttonText={translations('confirm')}
                  isLoading={isLoading}
                  // disable={Object.keys(props.errors).length}
                />
              </>
            )}
          </Formik>
        </View>
      </ScrollView>

      <ActionSheet
        title={translations('media.selectPrescription')}
        ref={actionSheetRef}
        options={[
          translations('media.chooseFromGallery'),
          translations('media.takePhoto'),
          translations('cancel'),
        ]}
        cancelButtonIndex={2}
        onPress={_actionSheetPress}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  wrapper: {
    backgroundColor: colors.mainBG,
    flex: 1,
    paddingHorizontal: sizes.containerPaddingH,
  },
  content: {
    width: '100%',
    marginTop: 100,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    width: '65%',
    marginTop: 13,
  },
  swContentStyle: {
    justifyContent: 'space-between',
    paddingBottom: 26,
  },
  button: {
    marginTop: 40,
  },
});
