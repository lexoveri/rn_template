import { connect } from 'react-redux';
import SignUp from './SignUp';
import { AUTH_SIGN_UP } from '~/modules/auth/actions';
import { isLoadingSelector } from '~/modules/requestsStatuses/selectors';

function mapStateToProps(state) {
  return {
    isLoading: isLoadingSelector(state, AUTH_SIGN_UP),
  };
}

const mapDispatchToProps = {
  createUser: AUTH_SIGN_UP.START.create,
};

export default connect(mapStateToProps, mapDispatchToProps)(SignUp);
