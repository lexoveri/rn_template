import React from 'react';
import { StyleSheet, View, SafeAreaView } from 'react-native';
import { translations } from '~/utils/i18n';

import { Button, Text } from '~/ui/components';
import { AuthStackTypes } from '~/navigation/main/types';
import { colors, sizes } from '~/ui/style';

type Props = {
  navigation: AuthStackTypes.SignInEmailScreenNavigationProp;
};

export default function({ navigation }: Props) {
  function navigateSignIn() {
    navigation.navigate('SignInStack', {
      screen: 'SignIn',
    });
  }

  function navigateSignUp() {
    navigation.navigate('SignUpStack');
  }
  return (
    <View style={styles.wrapper}>
      <SafeAreaView>
        <View style={styles.content}>
          <Text style={styles.text} gray center>
            {translations('welcomeScreen.description')}
          </Text>

          <View style={styles.buttonContainer}>
            <Button
              onPress={navigateSignIn}
              buttonText={translations('buttons.login')}
            />

            <Button
              onPress={navigateSignUp}
              white
              buttonText={translations('buttons.register')}
            />
          </View>
        </View>
      </SafeAreaView>
    </View>
  );
}

const styles = StyleSheet.create({
  wrapper: {
    backgroundColor: colors.mainBG,
    flex: 1,
    paddingHorizontal: sizes.containerPaddingH,
  },
  buttonsContainer: {
    marginBottom: 36,
  },
  buttonContainer: {
    marginBottom: 10,
  },
  text: {
    position: 'absolute',
    top: '50%',
    width: '60%',
  },
  content: {
    justifyContent: 'flex-end',
    alignContent: 'center',
    alignItems: 'center',
    height: '100%',
  },
});
