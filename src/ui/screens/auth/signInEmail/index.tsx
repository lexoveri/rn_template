import { connect } from 'react-redux';
import SignInEmail from './SignInEmail';
import { AUTH_SIGN_IN } from '~/modules/auth/actions';
import { isLoadingSelector } from '~/modules/requestsStatuses/selectors';

function mapStateToProps(state) {
  return {
    isLoading: isLoadingSelector(state, AUTH_SIGN_IN),
  };
}

const mapDispatchToProps = {
  login: AUTH_SIGN_IN.START.create,
};

export default connect(mapStateToProps, mapDispatchToProps)(SignInEmail);
