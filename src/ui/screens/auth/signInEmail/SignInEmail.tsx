import React, { useRef, useState } from 'react';
import {
  StyleSheet,
  View,
  ScrollView,
  KeyboardAvoidingView,
  Platform,
} from 'react-native';
import { translations } from '~/utils/i18n';

import { Input, Button, ScreenHeader } from '~/ui/components';

import { AuthStackTypes } from '~/navigation/main/types';
import { useKeyboardFocusSubscription } from '~/utils/customHooks';
import { colors, sizes } from '~/ui/style';
import { validateEmail } from '~/utils/string';

interface Props {
  navigation: AuthStackTypes.SignInEmailScreenNavigationProp;
  isLoading: boolean;
  login: () => any;
}

export default function({ navigation, login, isLoading }: Props) {
  const [email, setEmail] = useState('');
  const [ILId, setILId] = useState('');
  const [isEmailError, toggleEmailError] = useState(false);

  const keyboardOpened = useKeyboardFocusSubscription();

  const scrollViewRef = useRef();

  function sendEmail() {
    const isValid = validateEmail(email);
    if (!isValid) {
      toggleEmailError(true);
      return;
    }
    login({ email, password: ILId });
  }

  function onChangeEmail(text: string) {
    if (isEmailError) {
      toggleEmailError(false);
    }
    setEmail(text);
  }

  return (
    <View style={styles.wrapper}>
      <KeyboardAvoidingView
        style={{ flex: 1, paddingBottom: keyboardOpened ? 200 : 0 }}
        renderToHardwareTextureAndroid
      >
        <ScrollView
          ref={scrollViewRef}
          keyboardShouldPersistTaps="handled"
          onContentSizeChange={() => scrollViewRef.current.scrollToEnd()}
          showsHorizontalScrollIndicator={false}
          contentContainerStyle={[
            styles.swContentStyle,
            { flexGrow: keyboardOpened ? 0 : 1 },
          ]}
        >
          <View style={[styles.content]}>
            <ScreenHeader
              title={translations('emailLoginScreen.screenTitle')}
              description={translations('emailLoginScreen.screenDescription')}
            />

            <View style={styles.inputContainer}>
              <Input
                label={translations('email')}
                value={email}
                error={isEmailError}
                onChangeText={onChangeEmail}
                errorMessage={translations('messages.phoneNumberError')}
                keyboardType={
                  Platform.OS === 'android' ? 'numeric' : 'number-pad'
                }
              />

              <Input
                label={translations('password')}
                value={ILId}
                onChangeText={setILId}
                secureTextEntry
              />
            </View>
          </View>

          <Button
            onPress={sendEmail}
            buttonText={translations('send')}
            style={styles.button}
            isLoading={isLoading}
          />
        </ScrollView>
      </KeyboardAvoidingView>
    </View>
  );
}

const styles = StyleSheet.create({
  wrapper: {
    backgroundColor: colors.mainBG,
    flex: 1,
    paddingHorizontal: sizes.containerPaddingH,
  },
  content: {
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
  button: {
    marginTop: 40,
  },
  swContentStyle: {
    justifyContent: 'space-between',
    paddingBottom: 40,
  },
  inputContainer: {
    marginTop: '40%',
  },
});
