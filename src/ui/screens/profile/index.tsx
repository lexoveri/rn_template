import ProfilePage from './screens/profilePage';
import EditProfile from './screens/editProfile';

export { ProfilePage, EditProfile };
