import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Text } from '~/ui/components';

interface UserInfoItemProps {
  label?: string;
  text?: string;
}

export default function UserInfoItem({ label, text }: UserInfoItemProps) {
  if (!text) return null;
  return (
    <View style={styles.wrapper}>
      <Text small gray>
        {label}
      </Text>
      <Text primary h3 style={styles.text}>
        {text}
      </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  wrapper: {
    marginVertical: 20,
    width: '100%',
    height: 50,
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'flex-start',
  },
  text: {
    marginTop: 5,
  },
});
