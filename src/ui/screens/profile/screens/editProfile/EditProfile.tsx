import React, { useRef, useState } from 'react';
import {
  ScrollView,
  StyleSheet,
  View,
  KeyboardAvoidingView,
} from 'react-native';
import { translations } from '~/utils/i18n';

import { Input, Button, ScreenHeader } from '~/ui/components';

import { AuthStackTypes } from '~/navigation/main/types';
import { useKeyboardFocusSubscription } from '~/utils/customHooks';
import { colors, sizes } from '~/ui/style';
import { IUserData, UserUpdateBody } from '~/api/user/userTypes';
import { Formik } from 'formik';
import { validateEmail } from '~/utils/string';

interface EditProfileProps {
  navigation: AuthStackTypes.SignInEmailScreenNavigationProp;
  user: IUserData;
  updateData: (data: UserUpdateBody) => any;
  isLoading: boolean;
}

export default function EditProfile({
  user: { displayName, email, phoneNumber, photoURL },
  updateData,
  isLoading,
}: EditProfileProps) {
  const keyboardOpened = useKeyboardFocusSubscription();

  const scrollViewRef = useRef();

  function validate(values: IUserData) {
    const errors = {};

    if (!values.displayName || values.displayName.trim().length === 0) {
      errors.displayName = true;
    }

    if (!validateEmail(values.email)) {
      errors.email = true;
    }

    return errors;
  }

  function submit(values: UserUpdateBody) {
    updateData(values);
  }

  return (
    <View style={styles.wrapper}>
      <Formik
        enableReinitialize
        initialValues={{
          displayName,
          phoneNumber,
          email,
        }}
        // validate={validate}
        onSubmit={submit}
        validateOnBlur
        validateOnChange={false}
      >
        {props => (
          <KeyboardAvoidingView
            style={{ flex: 1, paddingBottom: keyboardOpened ? 200 : 0 }}
          >
            <ScrollView
              showsHorizontalScrollIndicator={false}
              ref={scrollViewRef}
              keyboardShouldPersistTaps="handled"
              onContentSizeChange={() => scrollViewRef.current.scrollToEnd()}
              contentContainerStyle={[
                styles.swContentStyle,
                { flexGrow: keyboardOpened ? 0 : 1 },
              ]}
            >
              <View style={styles.content}>
                <ScreenHeader
                  title={translations('profileTab.personalInfo')}
                  style={{ marginBottom: 30 }}
                />

                <Input
                  label={translations('displayName')}
                  value={props.values.displayName}
                  onChangeText={props.handleChange('displayName')}
                  error={props.errors.displayName}
                  blur={props.handleBlur('displayName')}
                />

                <Input
                  label={translations('email')}
                  value={props.values.email}
                  onChangeText={props.handleChange('email')}
                  error={props.errors.email}
                  blur={props.handleBlur('email')}
                />

                <Input
                  label={translations('phone')}
                  value={props.values.phoneNumber}
                  onChangeText={props.handleChange('phoneNumber')}
                  error={props.errors.phoneNumber}
                  blur={props.handleBlur('phoneNumber')}
                />
              </View>
              <Button
                style={styles.button}
                onPress={props.handleSubmit}
                buttonText={translations('save')}
                isLoading={isLoading}
              />
            </ScrollView>
          </KeyboardAvoidingView>
        )}
      </Formik>
    </View>
  );
}

const styles = StyleSheet.create({
  wrapper: {
    backgroundColor: colors.mainBG,
    flex: 1,
    paddingHorizontal: sizes.containerPaddingH,
  },
  content: {
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
  button: {
    marginTop: 40,
  },
  swContentStyle: {
    justifyContent: 'space-between',
    paddingBottom: 40,
  },
});
