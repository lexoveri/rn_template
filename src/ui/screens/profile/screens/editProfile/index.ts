import { connect } from 'react-redux';
import EditProfile from './EditProfile';
import { userDataSelector } from '~/modules/user/selectors';
import { RootState } from '~/modules/rootReducer';
import { USER_UPDATE } from '~/modules/user/actions';
import { isLoadingSelector } from '~/modules/requestsStatuses/selectors';

function mapStateToProps(state: RootState) {
  return {
    user: userDataSelector(state),
    isLoading: isLoadingSelector(state, USER_UPDATE),
  };
}

const mapDispatchToProps = {
  updateData: USER_UPDATE.START.create,
};

export default connect(mapStateToProps, mapDispatchToProps)(EditProfile);
