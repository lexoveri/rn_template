import React from 'react';
import { View, StyleSheet, ScrollView, TouchableOpacity } from 'react-native';
import { Text, ScreenHeader, Button } from '~/ui/components';
import { translations } from '~/utils/i18n';
import { useSafeArea } from 'react-native-safe-area-context';

import { TabNavTypes } from '~/navigation/main/types';

import { UserInfoItem } from '../../components';
import { colors, sizes } from '~/ui/style';
import { IUserState } from '~/modules/user/reducer';

interface ProfilePageProps {
  user: IUserState;
  navigation: TabNavTypes.ProfileInfoScreenNavigationProp;
  logout: () => any;
}

export default function ProfilePage({
  logout,
  user: { displayName, email, phoneNumber, photoURL },
  navigation,
}: ProfilePageProps) {
  const insets = useSafeArea();

  function editProfile() {
    navigation.navigate('ScreenStack', {
      screen: 'Edit'
    });
  }

  return (
    <View style={styles.wrapper}>
      <ScrollView
        showsHorizontalScrollIndicator={false}
        style={{
          width: '100%',
          flex: 1,
        }}
        contentContainerStyle={[
          styles.swContentStyle,
          { paddingTop: insets.top + sizes.headerHeight },
        ]}
      >
        <View style={styles.headerWrapper}>
          <ScreenHeader style={styles.screenHeader} title={displayName} />

          <Button
            buttonText={translations('edit')}
            white
            style={styles.editButton}
            onPress={editProfile}
          />
        </View>

        <View style={styles.userInfoBlock}>
          <UserInfoItem label={translations('email')} text={email} />

          <UserInfoItem label={translations('phone')} text={phoneNumber} />
        </View>

        <TouchableOpacity onPress={logout} style={styles.profileButton}>
          <Text error size={16} secondary>
            {translations('profileTab.logOut')}
          </Text>
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  wrapper: {
    backgroundColor: colors.mainBG,
    flex: 1,
    paddingHorizontal: sizes.containerPaddingH,
  },
  headerWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
    alignItems: 'flex-start',
  },
  screenHeader: {
    flex: 1,
  },
  editButton: {
    width: 80,
    height: 36,
  },
  userInfoBlock: {
    marginVertical: 35,
    width: '100%',
  },
  profileButton: {
    height: 50,
    width: '100%',
    borderBottomColor: '#D2D5DE',
    borderBottomWidth: 1,
    justifyContent: 'center',
  },
  swContentStyle: {
    paddingBottom: 40,
    // flexGrow: 1,
  },
});
