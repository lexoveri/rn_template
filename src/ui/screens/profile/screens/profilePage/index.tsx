import { connect } from 'react-redux';
import ProfilePage from './ProfilePage';
import { userDataSelector } from '~/modules/user/selectors';
import { RootState } from '~/modules/rootReducer';
import { USER_LOG_OUT } from '~/modules/user/actions';

function mapStateToProps(state: RootState) {
  return {
    user: userDataSelector(state),
  };
}

const mapDispatchToProps = {
  logout: USER_LOG_OUT.START.create,
};

export default connect(mapStateToProps, mapDispatchToProps)(ProfilePage);
