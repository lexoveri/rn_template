import { connect } from 'react-redux';
import TripsList from './PageTab2';

function mapStateToProps(state) {
  return {};
}

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(TripsList);
