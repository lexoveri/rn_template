const colors = {
  accent: '#55627F',
  primary: '#516282',
  secondary: '#385AB1',
  tertiary: '#8F98A9',
  black: '#323643',
  white: '#FFFFFF',
  gray: '#A6ADBB',
  darkGray: '#9197A8',
  border: 'rgba(202, 202, 202, 0.7)',
  hiddenBorder: 'rgba(202, 202, 202, 0.1)',
  placeholder: 'rgb(203, 205, 207)',
  red: '#E82B3A',
  error: '#D53F42',
  statusBar: {
    light: '#FFFFFF',
    dark: '#000',
  },
  buttonActive: '#172A4A',
  buttonDisabled: '#C5C9DC',

  mainBG: '#EDEFF6',

  activeTabIcon: '#2C5BB7',
  disableTabIcon: '#9198A7',
};

const sizes = {
  // global sizes
  small: 13,
  base: 16,
  font: 14,
  radius: 6,
  padding: 25,

  // font sizes
  h1: 32,
  h2: 21,
  h3: 17,
  title: 18,
  header: 16,
  body: 15,
  caption: 11,
  containerPaddingH: 16,
  headerHeight: 60,
};

const fonts = {
  h1: {
    fontSize: sizes.h1,
  },
  h2: {
    fontSize: sizes.h2,
  },
  h3: {
    fontSize: sizes.h3,
  },
  header: {
    fontSize: sizes.header,
  },
  title: {
    fontSize: sizes.title,
  },
  body: {
    fontSize: sizes.body,
  },
  caption: {
    fontSize: sizes.caption,
  },
  small: {
    fontSize: sizes.small,
  },
};

const borderParams = {
  borderRadius: 8,
  marginVertical: 8,
  borderWidth: 0.3,
  borderColor: colors.border,
};

export const datePickerStyles = {
  btnTextConfirm: {
    fontSize: 17,
    color: '#4C89FF',
    letterSpacing: -0.36,
  },
  btnConfirm: {
    backgroundColor: '#FBFBFB',
    width: '100%',
    justifyContent: 'flex-end',
  },
};

const shadowParams = {
  shadowColor: '#E1E3ED',
  shadowOffset: {
    width: 0,
    height: 9,
  },
  shadowOpacity: 1,
  shadowRadius: 12,

  elevation: 6,
};

const borderRadius = 12;

export { colors, sizes, fonts, borderParams, shadowParams, borderRadius };
