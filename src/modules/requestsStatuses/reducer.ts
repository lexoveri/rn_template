import head from 'lodash/head';
import last from 'lodash/last';

import { ACTION_DIVIDER, ACTIONS_SUBTYPES } from '../utils/actions';

const INTIAL_STATE = {};

function reduceAction(state, id, status, payload) {
  return id === undefined
    ? {
        status,
        payload,
        timestamp: new Date().getTime(),
      }
    : {
        ...state,
        [id]: {
          status,
          payload,
          timestamp: new Date().getTime(),
        },
      };
}

function reducer(state = INTIAL_STATE, action) {
  const actionParts = action.type.split(ACTION_DIVIDER);

  const status = last(actionParts);
  const majorType = head(actionParts);

  switch (status) {
    case ACTIONS_SUBTYPES.PRE_START:
    case ACTIONS_SUBTYPES.START:
    case ACTIONS_SUBTYPES.SUCCESS:
    case ACTIONS_SUBTYPES.FAILED:
      return {
        ...state,
        [majorType]: reduceAction(
          state[majorType],
          action.id,
          status,
          action.payload,
        ),
      };
  }

  return state;
}

export default reducer;
