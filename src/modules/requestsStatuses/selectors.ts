import lodashGet from 'lodash/get';

const requestsStatusesRootSelector = (state) => state.requestsStatuses;

export const getRequestsStatusForActionSelector = (state, action) =>
  lodashGet(
    requestsStatusesRootSelector(state),
    action.id !== undefined
      ? `${action.majorType}.${action.id}`
      : `${action.majorType}`,
  );

export const isLoadingSelector = (state, action) => {
  const requestData = getRequestsStatusForActionSelector(state, action);

  return !!(requestData && requestData.status === 'START');
};

export const isSuccessSelector = (state, action) => {
  const requestData = getRequestsStatusForActionSelector(state, action);

  return !!(requestData && requestData.status === 'SUCCESS');
};

export const getActionPayloadSelector = (state, action) => {
  const requestData = getRequestsStatusForActionSelector(state, action);

  return lodashGet(requestData, 'payload');
};

export const reasonFailedSelector = (state, action) => {
  const requestData = getRequestsStatusForActionSelector(state, action);

  return requestData && requestData.status === 'FAILED'
    ? requestData.payload.reason
    : undefined;
};

export const getSuccessPayloadSelector = (state, action) => {
  const requestData = getRequestsStatusForActionSelector(state, action);

  return requestData && requestData.status === 'SUCCESS'
    ? requestData.payload
    : undefined;
};

export const getActionTimestampSelector = (state, action) => {
  const requestData = getRequestsStatusForActionSelector(state, action);

  return lodashGet(requestData, 'timestamp', 0);
};

export const isActionSuccessAndNotOlderSelector = (
  state,
  action,
  notOlderThanMilliseconds = 5000,
) => {
  const timestamp = getActionTimestampSelector(state, action);

  return notOlderThanMilliseconds > new Date().getTime() - timestamp;
};
