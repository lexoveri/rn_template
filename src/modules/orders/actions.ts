import { createAction, ACTIONS_SUBTYPES } from '~/modules/utils/actions';

export const ORDER_CREATE = createAction('ORDER_CREATE', {
  [ACTIONS_SUBTYPES.START]: (orderData) => orderData,
  [ACTIONS_SUBTYPES.SUCCESS]: (products) => products,
  [ACTIONS_SUBTYPES.FAILED]: (reason) => ({
    reason,
  }),
});

export const ORDER_GET = createAction('ORDER_GET', {
  [ACTIONS_SUBTYPES.START]: (queries) => queries,
  [ACTIONS_SUBTYPES.SUCCESS]: (products) => products,
  [ACTIONS_SUBTYPES.FAILED]: (reason) => ({
    reason,
  }),
});

export const ORDERS_GET_ALL = createAction('ORDERS_GET_ALL', {
  [ACTIONS_SUBTYPES.START]: (queries) => queries,
  [ACTIONS_SUBTYPES.SUCCESS]: (products) => products,
  [ACTIONS_SUBTYPES.FAILED]: (reason) => ({
    reason,
  }),
});
