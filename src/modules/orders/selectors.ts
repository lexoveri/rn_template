import { RootState } from '~/modules/rootReducer';
import { createSelector } from 'reselect';

export const rootOrdersStateSelector = (state: RootState) => state.orders;

export const orderDataSelector = (state: RootState) =>
  rootOrdersStateSelector(state);

export const ordersKeysSelector = createSelector(orderDataSelector, (items) =>
  Object.keys(items),
);

export const orderItemSelector = (state: RootState, productUuid: string) =>
  orderDataSelector(state)[productUuid];
