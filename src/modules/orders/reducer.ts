import produce from 'immer';
import lodashMerge from 'lodash/merge';

import { IAction } from '~/modules/utils/modulesTypes';
import {
  CreateOrderResponse,
  GetOrderResponse,
  IOrder,
  OrdersResponse,
} from '~/api/orders/ordersTypes';

import {
  ORDERS_GET_ALL,
  ORDER_GET,
  ORDER_CREATE,
} from '~/modules/orders/actions';

export type IOrdersState = [IOrder & GetOrderResponse];

export interface IOrdersAction extends IAction {
  payload: OrdersResponse | GetOrderResponse | CreateOrderResponse;
}

const INITIAL_STATE: IOrdersState = {};

export default function (
  state = INITIAL_STATE,
  action: IOrdersAction,
): IOrdersState {
  return produce(state, (draft) => {
    switch (action.type) {
      case ORDERS_GET_ALL.SUCCESS.type:
      case ORDER_GET.SUCCESS.type:
      case ORDER_CREATE.SUCCESS.type:
        lodashMerge(draft, action.payload);
        break;
    }
  });
}
