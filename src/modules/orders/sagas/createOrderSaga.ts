import { put } from 'redux-saga/effects';

import api from '~/api';

import { ORDER_CREATE } from '../actions';
import { CreateOrderResult } from '~/api/orders/ordersTypes';
import { NavigationService } from '~/navigation';

export default function* createOrderSaga(action) {
  const { response, error }: CreateOrderResult = yield api.orders.createOrder(
    action.payload,
  );

  if (error) {
    console.log('createOrder error', error);

    yield put(ORDER_CREATE.FAILED.create(error.message));
  } else {
    console.log('createOrder response', response);

    NavigationService.navigate('AddAddressDelivery', {
      screen: 'OrderSuccessfully',
    });

    const result = {
      [response.data.id]: response.data,
    };

    yield put(ORDER_CREATE.SUCCESS.create(result));
  }
}
