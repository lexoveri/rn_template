import { put } from 'redux-saga/effects';

import api from '~/api';

import { ORDER_GET } from '../actions';
import { GetOrderResult, GetOrderBody } from '~/api/orders/ordersTypes';

export default function* getOrderSaga(action) {
  const queries: GetOrderBody = action.payload;

  const { response, error }: GetOrderResult = yield api.orders.getOrder(
    queries,
  );

  if (error) {
    console.log('getOrderSaga error', error);

    yield put(ORDER_GET.FAILED.create(error.message));
  } else {
    console.log('getOrderSaga response', response);

    const result = {
      [response.data.id]: response.data,
    };

    yield put(ORDER_GET.SUCCESS.create(result));
  }
}
