import { all, takeLatest } from 'redux-saga/effects';

import { ORDER_CREATE, ORDER_GET, ORDERS_GET_ALL } from '../actions';

import createOrderSaga from './createOrderSaga';
import getOrderSaga from './getOrderSaga';
import getOrdersSaga from './getOrdersSaga';

function* rootOrdersSaga() {
  yield all([
    takeLatest(ORDER_CREATE.START.type, createOrderSaga),
    takeLatest(ORDER_GET.START.type, getOrderSaga),
    takeLatest(ORDERS_GET_ALL.START.type, getOrdersSaga),
  ]);
}

export default rootOrdersSaga;
