import { put } from 'redux-saga/effects';

import api from '~/api';

import { ORDER_CREATE } from '../actions';
import { OrdersResult, OrdersBody } from '~/api/orders/ordersTypes';

export default function* getOrdersSaga(action) {
  const queries: OrdersBody = action.payload;

  const { response, error }: OrdersResult = yield api.orders.orders(queries);

  if (error) {
    console.log('getOrdersSaga error', error);

    yield put(ORDER_CREATE.FAILED.create(error.message));
  } else {
    console.log('getOrdersSaga response', response);

    const result = {};

    response.data.forEach((item) => {
      result[item.id] = item;
    });

    yield put(ORDER_CREATE.SUCCESS.create(result));
  }
}
