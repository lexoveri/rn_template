import { createAction, ACTIONS_SUBTYPES } from '~/modules/utils/actions';

export const STATE_REHYDRATION = createAction('STATE_REHYDRATION', {
  [ACTIONS_SUBTYPES.DONE]: true,
});
