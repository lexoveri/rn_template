import { RootState } from '~/modules/rootReducer';
import { IAppState } from './reducer';

export const rootApplicationSelector = (state: RootState): IAppState =>
  state.app;

export const errorMessagesSelector = (state: RootState) =>
  rootApplicationSelector(state).errors.map(e => e.reason);
