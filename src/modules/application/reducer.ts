import produce from 'immer';
import { IAction } from '~/modules/utils/modulesTypes';
import { ACTION_DIVIDER, ACTIONS_SUBTYPES } from '~/modules/utils/actions';

export interface IAppState {}

const INITIAL_STATE: IAppState = {};

export interface IAppAction extends IAction {
  payload: any;
}

export default function (state = INITIAL_STATE, action: IAppAction): IAppState {
  return produce(state, (draft) => {
    switch (action.type) {
    }
  });
}
