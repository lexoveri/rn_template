import { createAction, ACTIONS_SUBTYPES } from '~/modules/utils/actions';

export const APP_TRIGGER_FIRST_OPEN = createAction('APP_TRIGGER_FIRST_OPEN', {
  [ACTIONS_SUBTYPES.STATE]: (errors) => errors,
});
