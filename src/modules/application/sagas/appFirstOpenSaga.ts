import { put } from 'redux-saga/effects';
import { USER_GET_INFO } from '../../user/actions';

export default function* appFirstOpenSaga() {
  yield put(USER_GET_INFO.START.create());
}
