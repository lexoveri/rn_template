import { all, takeLatest } from 'redux-saga/effects';

import { APP_TRIGGER_FIRST_OPEN } from '../actions';

import appFirstOpenSaga from './appFirstOpenSaga';

function* rootApplicationSaga() {
  yield all([takeLatest(APP_TRIGGER_FIRST_OPEN.STATE.type, appFirstOpenSaga)]);
}

export default rootApplicationSaga;
