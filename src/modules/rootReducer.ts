import { combineReducers } from 'redux';
import { persistReducer } from 'redux-persist';
import AsyncStorage from '@react-native-community/async-storage';

import requestsStatusesReducer from './requestsStatuses/reducer';
import userReducer, { IUserState } from './user/reducer';
import ordersReducer, { IOrdersState } from './orders/reducer';
import appReducer, { IAppState } from './application/reducer';
import authReducer, { IAuthState } from '~/modules/auth/reducer';
import pushNotifications, {
  IPushNotificationState,
} from '~/modules/pushNotifications/reducer';

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  timeout: 120000,
  whitelist: [],
};

export interface RootState {
  app: IAppState;
  auth: IAuthState;
  user: IUserState;
  orders: IOrdersState;
  pushNotifications: IPushNotificationState;
}

export default persistReducer(
  persistConfig,
  combineReducers({
    app: appReducer,
    requestsStatuses: requestsStatusesReducer,
    auth: authReducer,
    user: userReducer,
    orders: ordersReducer,
    pushNotifications,
  }),
);
