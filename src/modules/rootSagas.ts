import { all } from 'redux-saga/effects';

import rootAuthSaga from '~/modules/auth/sagas';
import rootUserSaga from '~/modules/user/sagas';
import rootOrdersSaga from '~/modules/orders/sagas';
import rootApplicationSaga from '~/modules/application/sagas';

function* rootSaga() {
  yield all([
    rootAuthSaga(),
    rootUserSaga(),
    rootOrdersSaga(),
    rootApplicationSaga(),
  ]);
}

export default rootSaga;
