import { USER_GET_INFO, USER_UPDATE, USER_LOG_OUT } from './actions';
import produce from 'immer';
import lodashMerge from 'lodash/merge';
import storage from '@react-native-community/async-storage';
import { IAction } from '~/modules/utils/modulesTypes';
import {
  SingInResponse,
  SignUpResponse,
  GetAuthTokenResponse,
} from '~/api/auth/authTypes';
import { persistReducer } from 'redux-persist';

export interface IUserState {
  firstName: string;
  lastName: string;
  emailAddress: string;
  currency: string;
  mobile: string;
}

const INITIAL_STATE: IUserState = {
  firstName: '',
  lastName: '',
  emailAddress: '',
  currency: '',
  mobile: '',
};

export interface IUserAction extends IAction {
  payload: SignUpResponse | SingInResponse | GetAuthTokenResponse;
}

function reducer(state = INITIAL_STATE, action: IUserAction): IUserState {
  return produce(state, (draft) => {
    switch (action.type) {
      case USER_GET_INFO.SUCCESS.type:
      case USER_UPDATE.SUCCESS.type:
        lodashMerge(draft, action.payload);
        break;

      case USER_LOG_OUT.SUCCESS.type:
        return INITIAL_STATE;
    }
  });
}

const persistConfig = {
  key: 'user',
  storage,
  // timeout: 120000,
};

export default persistReducer(persistConfig, reducer);
