import { IUserState } from '~/modules/user/reducer';
import { RootState } from '~/modules/rootReducer';
import { createSelector } from 'reselect';

export const rootUserSelector = (state: RootState): IUserState => state.user;

export const userDataSelector = (state: RootState) => ({
  firstName: rootUserSelector(state).firstName,
  lastName: rootUserSelector(state).lastName,
  emailAddress: rootUserSelector(state).emailAddress,
  currency: rootUserSelector(state).currency,
  mobile: rootUserSelector(state).mobile,
});

export const isUserLoggedInSelector = createSelector(userDataSelector, (user) =>
  Object.values(user).every((values) => !Boolean(values)),
);
