import { createAction, ACTIONS_SUBTYPES } from '~/modules/utils/actions';

export const USER_DELETE_ADDRESS = createAction('USER_DELETE_ADDRESS', {
  [ACTIONS_SUBTYPES.START]: guid => guid,
  [ACTIONS_SUBTYPES.SUCCESS]: response => response,
  [ACTIONS_SUBTYPES.FAILED]: reason => ({
    reason,
  }),
});

export const USER_ADD_ADDRESS = createAction('USER_ADD_ADDRESS', {
  [ACTIONS_SUBTYPES.START]: address => address,
  [ACTIONS_SUBTYPES.SUCCESS]: address => address,
  [ACTIONS_SUBTYPES.FAILED]: reason => ({
    reason,
  }),
});

export const USER_SET_PRIMARY_ADDRESS = createAction('USER_ADD_ADDRESS', {
  [ACTIONS_SUBTYPES.STATE]: address => address,
});

export const USER_GET_INFO = createAction('USER_GET_INFO', {
  [ACTIONS_SUBTYPES.START]: true,
  [ACTIONS_SUBTYPES.SUCCESS]: user => user,
  [ACTIONS_SUBTYPES.FAILED]: reason => ({
    reason,
  }),
});

export const USER_ADDRESSES_LIST = createAction('USER_ADDRESSES_LIST', {
  [ACTIONS_SUBTYPES.START]: true,
  [ACTIONS_SUBTYPES.SUCCESS]: addresses => addresses,
  [ACTIONS_SUBTYPES.FAILED]: reason => ({
    reason,
  }),
});

export const USER_UPDATE = createAction('USER_UPDATE', {
  [ACTIONS_SUBTYPES.START]: updateData => updateData,
  [ACTIONS_SUBTYPES.SUCCESS]: data => data,
  [ACTIONS_SUBTYPES.FAILED]: reason => ({
    reason,
  }),
});

export const USER_LOG_OUT = createAction('USER_LOG_OUT', {
  [ACTIONS_SUBTYPES.START]: true,
  [ACTIONS_SUBTYPES.SUCCESS]: true,
  [ACTIONS_SUBTYPES.FAILED]: reason => ({
    reason,
  }),
});
