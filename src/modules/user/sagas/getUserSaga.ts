import { put } from 'redux-saga/effects';

import api from '~/api';

import { USER_GET_INFO } from '../actions';
import { UserResult } from '~/api/user/userTypes';

export default function* getUserSaga() {
  const { response, error }: UserResult = yield api.user.user();

  if (error) {
    console.log('getUserSaga error', error);

    yield put(USER_GET_INFO.FAILED.create(error.message));
  } else {
    console.log('getUserSaga response', response);

    yield put(USER_GET_INFO.SUCCESS.create(response));
  }
}
