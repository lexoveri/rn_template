import { all, takeLatest } from 'redux-saga/effects';

import getUserSaga from './getUserSaga';
import userUpdateSaga from './userUpdateSaga';
import userLogOutSaga from './userLogoutSaga';

import {
  USER_UPDATE,
  USER_GET_INFO,
  USER_LOG_OUT,
} from '~/modules/user/actions';

function* rootUserSaga() {
  yield all([
    takeLatest(USER_LOG_OUT.START.type, userLogOutSaga),
    takeLatest(USER_UPDATE.START.type, userUpdateSaga),
    takeLatest(USER_GET_INFO.START.type, getUserSaga),
  ]);
}

export default rootUserSaga;
