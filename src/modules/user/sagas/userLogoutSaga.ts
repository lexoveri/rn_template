import { put } from 'redux-saga/effects';

import api from '~/api';

import { USER_LOG_OUT } from '../actions';
import { NavigationService } from '~/navigation';

export default function* userLogOutSaga(action) {
  /*  const {
    response,
    error,
  }: UserAddressesListResult = yield api.user.userUpdate(action.payload);

  if (error) {
    console.log('userUpdateSaga error', error);

    yield put(USER_LOG_OUT.FAILED.create(error.message));
  } else {
    console.log('userUpdateSaga response', response);

    yield put(USER_LOG_OUT.SUCCESS.create());

    NavigationService.navigate('AuthStack');
  }*/
  NavigationService.navigate('AuthStack');
  yield put(USER_LOG_OUT.SUCCESS.create());
}
