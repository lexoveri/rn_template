import { put } from 'redux-saga/effects';

import api from '~/api';

import { USER_UPDATE } from '../actions';
import { UserAddressesListResult } from '~/api/user/userTypes';
import { NavigationService } from '~/navigation';

export default function* userUpdateSaga(action) {
  const {
    response,
    error,
  }: UserAddressesListResult = yield api.user.userUpdate(action.payload);

  if (error) {
    console.log('userUpdateSaga error', error);

    yield put(USER_UPDATE.FAILED.create(error.message));
  } else {
    console.log('userUpdateSaga response', response);

    yield put(USER_UPDATE.SUCCESS.create(response.data));

    NavigationService.goBack();
  }
}
