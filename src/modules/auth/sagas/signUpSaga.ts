import { put } from 'redux-saga/effects';

import api from '~/api';

import { AUTH_SIGN_UP } from '../actions';
import { SignUpResult } from '~/api/auth/authTypes';
import { SignUpUserData } from '~/api/utils/apiTypes';

export default function* signUpSaga(action) {
  const userData: SignUpUserData = action.payload;

  const { response, error }: SignUpResult = yield api.auth.signUp(userData);

  if (error) {
    console.log('signUpSaga error', error);

    yield put(AUTH_SIGN_UP.FAILED.create(error.message));
  } else {
    console.log('signUpSaga response', response);

    yield put(AUTH_SIGN_UP.SUCCESS.create(response));
  }
}
