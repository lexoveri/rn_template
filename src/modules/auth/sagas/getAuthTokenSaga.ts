import { put } from 'redux-saga/effects';

import api from '~/api';

import { AUTH_GET_TOKEN } from '../actions';
import { GetAuthTokenResult } from '~/api/auth/authTypes';
import { USER_GET_INFO } from '~/modules/user/actions';
import NavigationService from '~/navigation/utils/NavigationService';

export default function* getAuthTokenSaga(action) {
  const { response, error }: GetAuthTokenResult = yield api.auth.getAuthToken(
    action.payload,
  );

  if (error) {
    console.log('getAuthTokenSaga error', error);

    yield put(AUTH_GET_TOKEN.FAILED.create(error.message));
  } else {
    console.log('getAuthTokenSaga response', response);

    yield put(AUTH_GET_TOKEN.SUCCESS.create(response));

    yield put(USER_GET_INFO.START.create());

    NavigationService.navigate('TabNavigator');
  }
}
