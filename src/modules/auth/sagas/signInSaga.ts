import { put } from 'redux-saga/effects';

import api from '~/api';

import { AUTH_SIGN_IN } from '../actions';
import { SingInResult } from '~/api/auth/authTypes';

export default function* signInSaga(action) {
  const { response, error }: SingInResult = yield api.auth.singIn(
    action.payload,
  );

  if (error) {
    console.log('signInSaga error', error);

    yield put(AUTH_SIGN_IN.FAILED.create(error.message));
  } else {
    console.log('signInSaga response', response);

    yield put(AUTH_SIGN_IN.SUCCESS.create(response));
  }
}
