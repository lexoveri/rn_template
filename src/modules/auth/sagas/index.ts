import { all, takeLatest } from 'redux-saga/effects';

import getAuthTokenSaga from './getAuthTokenSaga';
import signUpSaga from './signUpSaga';
import signInSaga from './signInSaga';

import {
  AUTH_GET_TOKEN,
  AUTH_SIGN_IN,
  AUTH_SIGN_UP,
} from '~/modules/auth/actions';

function* rootAuthSaga() {
  yield all([
    takeLatest(AUTH_GET_TOKEN.START.type, getAuthTokenSaga),
    takeLatest(AUTH_SIGN_UP.START.type, signUpSaga),
    takeLatest(AUTH_SIGN_IN.START.type, signInSaga),
  ]);
}

export default rootAuthSaga;
