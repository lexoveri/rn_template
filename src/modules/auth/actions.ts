import { createAction, ACTIONS_SUBTYPES } from '~/modules/utils/actions';

export const AUTH_GET_TOKEN = createAction('AUTH_GET_TOKEN', {
  [ACTIONS_SUBTYPES.START]: (data) => data,
  [ACTIONS_SUBTYPES.SUCCESS]: (token) => token,
  [ACTIONS_SUBTYPES.FAILED]: (reason) => ({
    reason,
  }),
});

export const AUTH_SIGN_IN = createAction('AUTH_SIGN_IN', {
  [ACTIONS_SUBTYPES.START]: (data) => data,
  [ACTIONS_SUBTYPES.SUCCESS]: (token) => token,
  [ACTIONS_SUBTYPES.FAILED]: (reason) => ({
    reason,
  }),
});

export const AUTH_SIGN_UP = createAction('AUTH_SIGN_UP', {
  [ACTIONS_SUBTYPES.START]: (data) => data,
  [ACTIONS_SUBTYPES.SUCCESS]: (data) => data,
  [ACTIONS_SUBTYPES.FAILED]: (reason) => ({
    reason,
  }),
});
