import { AUTH_GET_TOKEN } from '~/modules/auth/actions';
import produce from 'immer';
import { IAction } from '~/modules/utils/modulesTypes';
import {
  SingInResponse,
  SignUpResponse,
  GetAuthTokenResponse,
} from '~/api/auth/authTypes';
import storage from '@react-native-community/async-storage';
import { persistReducer } from 'redux-persist';
import { USER_LOG_OUT } from '~/modules/user/actions';

import lodashMerge from 'lodash/merge';

export interface IAuthState {
  accessToken: string;
  expiresIn: number | null;
  refreshToken: string;
}

const INITIAL_STATE: IAuthState = {
  accessToken: '',
  expiresIn: null,
  refreshToken: '',
};

export interface IAuthAction extends IAction {
  payload: SignUpResponse | SingInResponse | GetAuthTokenResponse;
}

function reducer(state = INITIAL_STATE, action: IAuthAction): IAuthState {
  return produce(state, (draft) => {
    switch (action.type) {
      case AUTH_GET_TOKEN.SUCCESS.type:
        lodashMerge(draft, action.payload);
        break;

      case USER_LOG_OUT.SUCCESS.type:
        return INITIAL_STATE;
    }
  });
}

const persistConfig = {
  key: 'auth',
  storage,
};

export default persistReducer(persistConfig, reducer);
