import { RootState } from '~/modules/rootReducer';
import { IAuthState } from '~/modules/auth/reducer';

export const rootUserSelector = (state: RootState): IAuthState => state.auth;

export const isShowVerifyInputSelector = (state: RootState) =>
  rootUserSelector(state).showVerifyCode;

export const authRefreshTokenSelector = (state: RootState) =>
  rootUserSelector(state).refreshToken;

export const authAccessTokenToken = (state: RootState) =>
  rootUserSelector(state).accessToken;

export const authIsUserLoggedSelecor = (state: RootState) =>
  !!rootUserSelector(state).accessToken;
