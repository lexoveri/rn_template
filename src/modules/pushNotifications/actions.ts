import { createAction } from '~/modules/utils/actions';

export const PUSH_NOTIFICATION_REQUEST_TOKEN = createAction(
  'PUSH_NOTIFICATION_REQUEST_TOKEN',
  {
    START: true,
    SUCCESS: (token) => ({ token }),
    FAILED: true,
  },
);

export const PUSH_NOTIFICATION_UPDATE_TOKEN = createAction(
  'PUSH_NOTIFICATION_UPDATE_TOKEN',
  {
    START: (token) => ({ token }),
    SUCCESS: (token) => ({ token }),
    FAILED: true,
  },
);

export const PUSH_NOTIFICATION_START_WATCH_UPDATING_TOKEN = createAction(
  'PUSH_NOTIFICATION_START_WATCH_UPDATING_TOKEN',
  {
    START: true,
  },
);
