const rootSelector = (state) => state.pushNotifications;

export const getPushNotificationTokenSelector = (state) =>
  rootSelector(state).pushNotificationToken;
