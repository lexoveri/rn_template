import { put } from 'redux-saga/effects';

import { PUSH_NOTIFICATION_UPDATE_TOKEN } from '../actions';

import api from '~/api';

export default function* updatePushTokenSaga(action) {
  const {
    response,
    error,
  } = yield api.pushNotifications.updatePushNotificationToken(
    action.payload.token,
  );

  if (error) {
    console.log('updatePushTokenSaga error', error);

    yield put(PUSH_NOTIFICATION_UPDATE_TOKEN.FAILED.create(error.message));
  } else {
    console.log('updatePushTokenSaga response', response);

    yield put(
      PUSH_NOTIFICATION_UPDATE_TOKEN.SUCCESS.create(action.payload.token),
    );
  }
}
