import { put, select } from 'redux-saga/effects';
import messaging from '@react-native-firebase/messaging';
import {
  PUSH_NOTIFICATION_REQUEST_TOKEN,
  PUSH_NOTIFICATION_START_WATCH_UPDATING_TOKEN,
} from '~/modules/pushNotifications/actions';
import { getPushNotificationTokenSelector } from '~/modules/pushNotifications/selectors';

export default function* requestPushTokenSaga() {
  console.log('requestPushTokenSaga start');
  const pushNotificationToken = yield select(getPushNotificationTokenSelector);

  if (!pushNotificationToken) {
    try {
      const permissionAuthorizationStatus = yield messaging().hasPermission();

      console.log(
        'Has push messaging permissions: ',
        permissionAuthorizationStatus,
      );

      console.log('statuses', messaging.AuthorizationStatus);

      if (
        [
          messaging.AuthorizationStatus.AUTHORIZED,
          messaging.AuthorizationStatus.PROVISIONAL,
        ].includes(permissionAuthorizationStatus)
      ) {
        if (!messaging().isDeviceRegisteredForRemoteMessages) {
          yield messaging().registerDeviceForRemoteMessages();
        }
        console.log('Asking for token ');

        const token = yield messaging().getToken();

        console.log('Got token: ', token);

        yield put(PUSH_NOTIFICATION_REQUEST_TOKEN.SUCCESS.create(token));
      } else {
        console.log('Asking for permission');

        const permissionStatus = yield messaging().requestPermission();

        console.log('Permission response from the user: ', permissionStatus);

        if (permissionStatus === messaging.AuthorizationStatus.AUTHORIZED) {
          if (!messaging().isDeviceRegisteredForRemoteMessages) {
            yield messaging().registerDeviceForRemoteMessages();
          }
          const token = yield messaging().getToken();

          console.log('getToken: ', token);

          yield put(PUSH_NOTIFICATION_REQUEST_TOKEN.SUCCESS.create(token));
        }
      }
    } catch (error) {
      console.log('firebase push token error ', error);
      yield put(PUSH_NOTIFICATION_REQUEST_TOKEN.FAILED.create(error));
    }
  }

  yield put(PUSH_NOTIFICATION_START_WATCH_UPDATING_TOKEN.START.create());
}
