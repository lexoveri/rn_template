import { all, takeLatest } from 'redux-saga/effects';

import {
  PUSH_NOTIFICATION_REQUEST_TOKEN,
  PUSH_NOTIFICATION_UPDATE_TOKEN,
  PUSH_NOTIFICATION_START_WATCH_UPDATING_TOKEN,
} from '~/modules/pushNotifications/actions';

import requestPushTokenSaga from './requestPushTokenSaga';
import watchPushNotificationTokenRefreshSaga from './watchPushNotificationTokenRefreshSaga';
import updatePushTokenSaga from './updatePushTokenSaga';

export default function* rootSaga() {
  yield all([
    watchPushNotificationTokenRefreshSaga(),
    takeLatest(
      PUSH_NOTIFICATION_REQUEST_TOKEN.START.type,
      requestPushTokenSaga,
    ),
    takeLatest(
      [
        PUSH_NOTIFICATION_UPDATE_TOKEN.START.type,
        PUSH_NOTIFICATION_REQUEST_TOKEN.SUCCESS.type,
      ],
      updatePushTokenSaga,
    ),
    takeLatest(
      PUSH_NOTIFICATION_START_WATCH_UPDATING_TOKEN.START.type,
      watchPushNotificationTokenRefreshSaga,
    ),
  ]);
}
