import { take, call, put } from 'redux-saga/effects';
import { eventChannel } from 'redux-saga';
import firebase from '@react-native-firebase/app';
import { PUSH_NOTIFICATION_UPDATE_TOKEN } from '~/modules/pushNotifications/actions';

const createSubscription = () =>
  eventChannel((emitter) => {
    const onTokenRefreshListener = firebase
      .messaging()
      .onTokenRefresh((fcmToken) => emitter(fcmToken));

    return onTokenRefreshListener;
  });

export default function* watchPushNotificationTokenRefreshSaga() {
  console.log('start watching PushNotificationTokenRefresh');
  const subscriptionChanel = yield call(createSubscription);
  while (true) {
    const fcmToken = yield take(subscriptionChanel);
    yield put(PUSH_NOTIFICATION_UPDATE_TOKEN.START.create(fcmToken));
  }
}
