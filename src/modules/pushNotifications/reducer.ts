import produce from 'immer';
import { persistReducer } from 'redux-persist';
import AsyncStorage from '@react-native-community/async-storage';

import {
  PUSH_NOTIFICATION_REQUEST_TOKEN,
  PUSH_NOTIFICATION_UPDATE_TOKEN,
} from './actions';

const persistConfig = {
  timeout: 120000,
  key: 'auth',
  storage: AsyncStorage,
};

export interface IPushNotificationState {
  pushNotificationToken: number | null;
}

const INITIAL_STATE: IPushNotificationState = {
  pushNotificationToken: null,
};

function reducer(state = INITIAL_STATE, action) {
  return produce(state, (draft) => {
    switch (action.type) {
      case PUSH_NOTIFICATION_REQUEST_TOKEN.SUCCESS.type:
      case PUSH_NOTIFICATION_UPDATE_TOKEN.SUCCESS.type:
        draft.pushNotificationToken = action.payload.token;
        break;
    }
  });
}

export default persistReducer(persistConfig, reducer);
