export const ACTION_DIVIDER = ' -> ';

export enum ACTIONS_SUBTYPES {
  PRE_START = 'PRE_START',
  START = 'START',
  SUCCESS = 'SUCCESS',
  FAILED = 'FAILED',

  STATE = 'STATE',

  DONE = 'DONE',

  SUBSCRIBE = 'SUBSCRIBE',
  UNSUBSCRIBE = 'UNSUBSCRIBE',
  UNDO = 'UNDO',
}

type ID = string | number | undefined | null;

interface CreateFunction {
  type: string;
  payload?: any;
  id: ID;
}

interface ImportActionPart {
  [key: string]: boolean | Function;
}

interface ActionPart<T> {
  id: ID;
  type: string;
  create: (val?: any) => CreateFunction;
}

interface ActionObject {
  majorType: string;
  id: ID;
  type?: string;
}

type GenerateActionStructure<T> = {
  [K in keyof T]: ActionPart<T[K]>;
}; /*&
  ActionObject;*/

type ActionResult<S, M> = M extends boolean
  ? (id?: ID) => GenerateActionStructure<S>
  : GenerateActionStructure<S>;

export function createAction<
  T extends string,
  S extends ImportActionPart,
  M = undefined
>(type: T, structure: S, isMultiInstanceProcess?: M): ActionResult<S, M> {
  function actionGenerator(actionId?: ID) {
    const actionObject: GenerateActionStructure<S> = {
      majorType: type,
      id: actionId,
    };

    if (structure) {
      Object.keys(structure).forEach(<K extends keyof S>(actionPart: K) => {
        actionObject[actionPart] = {};

        const actionPartType = `${type}${ACTION_DIVIDER}${actionPart.toUpperCase()}`;

        actionObject[actionPart].type = actionPartType;
        actionObject[actionPart].id = actionId;

        if (structure[actionPart]) {
          if (structure[actionPart] instanceof Function) {
            // Use function provided by consumer

            actionObject[actionPart].create = (...params) => ({
              type: actionPartType,
              payload: structure[actionPart](...params),
              id: actionId,
            });
          } else {
            // Use default creator without payload

            actionObject[actionPart].create = () => ({
              type: actionPartType,
              id: actionId,
            });
          }
        }

        actionObject[actionPart] = Object.freeze(actionObject[actionPart]);
      });
    } else {
      actionObject.type = type;
      actionObject.id = actionId;
    }

    return Object.freeze(actionObject);
  }

  return isMultiInstanceProcess ? actionGenerator : actionGenerator();
}
