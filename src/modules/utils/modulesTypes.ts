export interface IAction {
  id: string | number | undefined;
  type: string;
  majorType: string;
}
